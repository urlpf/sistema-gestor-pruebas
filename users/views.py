from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import RegisterForm
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User, Group
from proymod.decorators import group_required

# Create your views here.

# Registro de nuevos usuarios


@group_required('TestManager')
def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(
                request, f'La cuenta {username} ha sido registrada, el usuario podrá acceder al sistema')
            return redirect('home')
    else:
        form = RegisterForm
    return render(request, 'users/register.html', {'form': form})

# Login


def user_login(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                messages.info(request, f"Has iniciado sesión como {username}.")
                return redirect("bienvenida")
            else:
                messages.info(
                    request, "Nombre de usuario y/o contraseña incorrectos.")
        form = AuthenticationForm
        context = {"form": form}
        return render(request, "users/login.html", context)

# Pagina de perfil


@login_required
def profilepage(request):
    groups = request.user.groups.all()
    context = {
        'groups': groups,
    }
    return render(request, 'users/profile.html', context)
