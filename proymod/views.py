from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.core.paginator import Paginator
from django.contrib import messages
from .forms import NuevoProyecto, ActualizarProyecto, NuevoModulo, ActualizarModulo
from .models import Proyecto, Modulo
from guiprub.models import GuionPruebas, CasoPrueba, Defecto, ResultadoCasoPrueba
from django.utils import timezone
from django.urls import reverse
from .decorators import group_required

# Mensajes de este módulo
mensajes = {
    '001': 'El nombre ingresado ya está registrado en el sistema. Por favor ingresa otro nombre. ',
    '002': 'Elemento registrado exitosamente.',
    '003': 'Elemento modificado exitosamente',
    '004': 'Elemento eliminado exitosamente',
    '005': 'Operación no realizada',

}

# Indice de proyectos


@login_required(login_url="/usuarios/login")
def index(request):
    proyect_objects = Proyecto.objects.all()

    # Codigo para buscar
    item_name = request.GET.get('item_name')
    if item_name != '' and item_name is not None:
        proyect_objects = proyect_objects.filter(nombre__icontains=item_name)

    # Codigo del paginador
    paginator = Paginator(proyect_objects, 5)
    page = request.GET.get('page')
    proyect_objects = paginator.get_page(page)

    context = {
        'proyect_objects': proyect_objects,
    }

    return render(request, 'proymod/index.html', context)

# Crear nuevos proyectos


@login_required(login_url="/usuarios/login")
@group_required('TestManager')
def crear(request):
    if request.method == 'POST':
        form = NuevoProyecto(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.nombre = form.cleaned_data['nombre']
            post.fecha = timezone.datetime.now()
            post.usuario = request.user
            post.save()
            messages.success(request, mensajes.get("002"))
            return redirect('index')
        else:
            messages.warning(
                request, mensajes.get("005"))
    else:
        form = NuevoProyecto()

    return render(request, 'proymod/crear.html', {'form': form})
    # return render(request, 'proymod/crear.html')

# Ir a detalle de proyecto


@login_required(login_url="/usuarios/login")
def detalle(request, proy_id):
    proyecto = get_object_or_404(Proyecto, pk=proy_id)
    context = {
        'proyecto': proyecto,
    }
    return render(request, 'proymod/detalle.html', context)

# Editar proyecto


@login_required(login_url="/usuarios/login")
@group_required('TestManager')
def actualizar(request, proy_id):
    proyecto = Proyecto.objects.get(id=proy_id)
    if request.method == 'POST':
        form = ActualizarProyecto(request.POST or None, instance=proyecto)
        if form.is_valid():
            form.save()
            messages.success(request, mensajes.get("003"))
            return redirect('index')
        else:
            messages.warning(request, mensajes.get("005"))
    else:
        form = ActualizarProyecto(instance=proyecto)
    return render(request, 'proymod/actualizar.html', {'form': form, 'proyecto': proyecto})

# Borrar proyecto


@login_required(login_url="/usuarios/login")
@group_required('TestManager')
def borrar(request, proy_id):
    proyecto = Proyecto.objects.get(id=proy_id)
    if request.method == 'POST':
        proyecto.delete()
        messages.success(request, mensajes.get("004"))
        return redirect('index')

    return render(request, 'proymod/borrar.html', {'proyecto': proyecto})

# Indice de modulos


@login_required(login_url="/usuarios/login")
def index_modulos(request, proy_id):
    proyecto = Proyecto.objects.get(id=proy_id)

    modul_objects = Modulo.objects.filter(proyecto=proyecto.id)

    # Codigo para buscar
    modul_name = request.GET.get('modul_name')
    if modul_name != '' and modul_name is not None:
        modul_objects = modul_objects.filter(nombre__icontains=modul_name)

    # Codigo del paginador
    paginator = Paginator(modul_objects, 5)
    page = request.GET.get('page')
    modul_objects = paginator.get_page(page)

    context = {
        'modul_objects': modul_objects,
        'proyecto': proyecto,
    }
    return render(request, 'proymod/index_modulos.html', context)

# Crear Modulo


@login_required(login_url="/usuarios/login")
@group_required('TestManager')
def crear_modulo(request, proy_id):
    proyecto = Proyecto.objects.get(id=proy_id)
    proyecto_id = proyecto.id
    if request.method == 'POST':
        form = NuevoModulo(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.nombre = form.cleaned_data['nombre']
            post.proyecto = proyecto
            post.save()
            messages.success(request, mensajes.get("002"))
            return redirect('modulos', proy_id=proyecto_id)
        else:
            messages.warning(request, mensajes.get("005"))
    else:
        form = NuevoModulo()

    return render(request, 'proymod/crear_modulo.html', {'form': form, 'proyecto': proyecto})


@login_required(login_url="/usuarios/login")
def detalle_modulo(request, modulo_id):
    modulo = get_object_or_404(Modulo, pk=modulo_id)
    proyecto = modulo.proyecto
    context = {
        'modulo': modulo,
        'proyecto': proyecto
    }
    return render(request, 'proymod/detalle_modulo.html', context)

# Editar Modulo


@login_required(login_url="/usuarios/login")
@group_required('TestManager')
def actualizar_modulo(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    proyecto = Proyecto.objects.get(nombre=modulo.proyecto)
    proyecto_id = proyecto.id
    if request.method == 'POST':
        form = ActualizarModulo(request.POST or None, instance=modulo)
        if form.is_valid():
            form.save()
            messages.success(request, mensajes.get("003"))
            return redirect('modulos', proy_id=proyecto_id)
        else:
            messages.warning(
                request, mensajes.get("005"))
    else:
        form = ActualizarModulo(instance=modulo)
    return render(request, 'proymod/actualizar_modulo.html', {'form': form, 'modulo': modulo})

# Borrar Modulo


@login_required(login_url="/usuarios/login")
@group_required('TestManager')
def borrar_modulo(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    proyecto = Proyecto.objects.get(nombre=modulo.proyecto)
    proyecto_id = proyecto.id

    if request.method == 'POST':
        modulo.delete()
        messages.success(request, mensajes.get("004"))
        return redirect('modulos', proy_id=proyecto_id)

    return render(request, 'proymod/borrar_modulo.html', {'modulo': modulo})


@login_required(login_url="/usuarios/login")
@group_required('TestManager')
def reportes_index(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    proyecto = Proyecto.objects.get(nombre=modulo.proyecto)
    guion_objects = GuionPruebas.objects.filter(modulo=modulo)
    guion_objects_total = guion_objects.count()
    cp_objects = CasoPrueba.objects.filter(
        guion__in=guion_objects)
    cp_objects_total = CasoPrueba.objects.filter(
        guion__in=guion_objects).count()
    result_objects = ResultadoCasoPrueba.objects.filter(
        caso_prueba__in=cp_objects)
    defect_objects = Defecto.objects.filter(
        resultado__in=result_objects).exclude(estado="Liberado por QA")
    defect_objects_total = defect_objects.count()
    if (guion_objects_total < 1) or (cp_objects_total < 1):
        bool_rep = False
    else:
        bool_rep = True
    if defect_objects_total < 1:
        bool_def = False
    else:
        bool_def = True
    context = {
        'modulo': modulo,
        'proyecto': proyecto,
        'bool_rep': bool_rep,
        'bool_def': bool_def,
    }
    return render(request, 'proymod/reportes.html', context)
