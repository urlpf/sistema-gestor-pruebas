from django import forms
from .models import Proyecto, Modulo


class NuevoProyecto(forms.ModelForm):
    class Meta:
        model = Proyecto
        fields = ['nombre', 'descripcion']

    def clean(self):
        try:
            sc = Proyecto.objects.get(
                nombre=self.cleaned_data['nombre'])
            if not self.instance.pk:
                raise forms.ValidationError(
                    "Nombre " + str(sc) + " ya esta registrado")
            elif self.instance.pk != sc.pk:
                raise forms.ValidationError(
                    "Cambio no permitido, el nombre coincide con otro registro")
        except Proyecto.DoesNotExist:
            pass
        return self.cleaned_data


class ActualizarProyecto(forms.ModelForm):
    class Meta:
        model = Proyecto
        fields = ['nombre', 'status', 'descripcion']

    def clean(self):
        try:
            sc = Proyecto.objects.get(
                nombre=self.cleaned_data['nombre'])
            if not self.instance.pk:
                raise forms.ValidationError(
                    "Nombre " + str(sc) + " ya esta registrado")
            elif self.instance.pk != sc.pk:
                raise forms.ValidationError(
                    "Cambio no permitido, el nombre coincide con otro registro")
        except Proyecto.DoesNotExist:
            pass
        return self.cleaned_data


class NuevoModulo(forms.ModelForm):
    class Meta:
        model = Modulo
        fields = ['nombre', 'descripcion']
        exclude = ['proyecto']

    def clean(self):
        try:
            sc = Modulo.objects.get(
                nombre=self.cleaned_data['nombre'])
            if not self.instance.pk:
                raise forms.ValidationError(
                    "Nombre " + str(sc) + " ya esta registrado")
            elif self.instance.pk != sc.pk:
                raise forms.ValidationError(
                    "Cambio no permitido, el nombre coincide con otro registro")
        except Modulo.DoesNotExist:
            pass
        return self.cleaned_data


class ActualizarModulo(forms.ModelForm):
    class Meta:
        model = Modulo
        fields = ['nombre', 'status', 'descripcion']

    def clean(self):
        try:
            sc = Modulo.objects.get(
                nombre=self.cleaned_data['nombre'])
            if not self.instance.pk:
                raise forms.ValidationError(
                    "Nombre " + str(sc) + " ya esta registrado")
            elif self.instance.pk != sc.pk:
                raise forms.ValidationError(
                    "Cambio no permitido, el nombre coincide con otro registro")
        except Modulo.DoesNotExist:
            pass
        return self.cleaned_data
