from django.urls import path, include
from . import views

urlpatterns = [
    # Proyectos
    # Indice proyecto
    path('', views.index, name="index"),
    # Crear proyecto
    path('crear/', views.crear, name="crear"),
    # Detalle proyecto
    path('<int:proy_id>/', views.detalle, name="detalle"),
    # Actualizar proyecto
    path('actualizar/<int:proy_id>/', views.actualizar, name='actualizar'),
    # Borrar proyecto
    path('borrar/<int:proy_id>/', views.borrar, name='borrar'),
    # Modulos
    # indice modulos
    path('modulos/<int:proy_id>/', views.index_modulos, name="modulos"),
    # crear modulo
    path('modulos/crear/<int:proy_id>/',
         views.crear_modulo, name="crear_modulo"),
    # Detalle de modulo
    path('modulos/detalle/<int:modulo_id>/',
         views.detalle_modulo, name="modulo_detalle"),
    # Actualizar modulo
    path('modulos/actualizar/<int:modulo_id>/',
         views.actualizar_modulo, name='actualizar_modulo'),
    # Borrar modulo
    path('modulos/borrar/<int:modulo_id>/',
         views.borrar_modulo, name='borrar_modulo'),
    # Guiones
    path('modulos/guiones/', include('guiprub.urls')),
    # Test Suites
    path('modulos/testsuites/', include('testsuite.urls')),
    # Reportes
    path('modulos/reportes/<int:modulo_id>/',
         views.reportes_index, name='reportes_index'),
]
