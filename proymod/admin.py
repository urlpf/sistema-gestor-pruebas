from django.contrib import admin
from .models import Proyecto, Modulo

# Register your models here.
admin.site.register(Proyecto)
admin.site.register(Modulo)
