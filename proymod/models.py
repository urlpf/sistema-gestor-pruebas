from django.db import models
from django.contrib.auth.models import User

# Create your models here.
STATUS_PROYECTO = [
    ('Finalizado', 'Finalizado'),
    ('En progreso', 'En progreso'),
]


class Proyecto(models.Model):
    nombre = models.CharField(max_length=200, unique=True)
    fecha = models.DateTimeField()
    descripcion = models.TextField(max_length=1000)
    status = models.CharField(
        max_length=150, default="En progreso", choices=STATUS_PROYECTO)
    usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.nombre

    def fecha_resumida(self):
        return self.fecha.strftime('%b %e %Y')

    def nombre_abre(self):
        nombre_rec = self.nombre[0:1]
        nombre_cap = nombre_rec.capitalize() + "-"
        return nombre_cap


class Modulo(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField(max_length=1000)
    status = models.CharField(
        max_length=150, default="En progreso", choices=STATUS_PROYECTO)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)

    class Meta:
        unique_together = ["nombre", "proyecto"]

    def __str__(self):
        return self.nombre
