from django import forms
from .models import BasePrueba, GuionPruebas, CasoPrueba, Defecto, ResultadoCasoPrueba


class BasePruebaForm(forms.ModelForm):
    class Meta:
        model = BasePrueba
        fields = ('base_nombre',)


class ActualizarGuionForm(forms.ModelForm):
    class Meta:
        model = GuionPruebas
        fields = ['nombre', 'trayectoria_p', 'trayectoria_al',
                  'entradas', 'salidas', 'errores', ]
        exclude = ['modulo']

    def clean(self):
        try:
            sc = GuionPruebas.objects.get(
                nombre=self.cleaned_data['nombre'])
            if not self.instance.pk:
                raise forms.ValidationError(
                    "Nombre " + str(sc) + " ya esta registrado")
            elif self.instance.pk != sc.pk:
                raise forms.ValidationError(
                    "Cambio no permitido, el nombre coincide con otro registro")
        except GuionPruebas.DoesNotExist:
            pass
        return self.cleaned_data


class RegistrarResultadoForm(forms.ModelForm):
    class Meta:
        model = ResultadoCasoPrueba
        fields = ['fecha', 'resultado', 'observaciones', ]
        exclude = ['caso_prueba', ]
        labels = {
            "fecha": "Fecha (aaaa-mm-dd)",
        }


class ActualizarResultadoForm(forms.ModelForm):
    class Meta:
        model = ResultadoCasoPrueba
        fields = ['fecha', 'resultado', 'observaciones', ]
        exclude = ['caso_prueba', ]
        labels = {
            "fecha": "Fecha (aaaa-mm-dd)",
        }


class RegistrarDefectoForm(forms.ModelForm):
    class Meta:
        model = Defecto
        fields = ['titulo', 'tipo_defecto',
                  'prioridad', 'descripcion', 'evidencia']
        exclude = ['casoprueba']

    def clean(self):
        try:
            sc = Defecto.objects.get(
                titulo=self.cleaned_data['titulo'])
            if not self.instance.pk:
                raise forms.ValidationError(
                    "titulo " + str(sc) + " ya esta registrado")
            elif self.instance.pk != sc.pk:
                raise forms.ValidationError(
                    "Cambio no permitido, el nombre coincide con otro registro")
        except Defecto.DoesNotExist:
            pass
        return self.cleaned_data


class ActualizarDefectoForm(forms.ModelForm):
    class Meta:
        model = Defecto
        fields = ['titulo', 'estado', 'tipo_defecto',
                  'prioridad', 'descripcion', 'evidencia']
        exclude = ['caso_prueba']

    def clean(self):
        try:
            sc = Defecto.objects.get(
                titulo=self.cleaned_data['titulo'])
            if not self.instance.pk:
                raise forms.ValidationError(
                    "titulo " + str(sc) + " ya esta registrado")
            elif self.instance.pk != sc.pk:
                raise forms.ValidationError(
                    "Cambio no permitido, el nombre coincide con otro registro")
        except Defecto.DoesNotExist:
            pass
        return self.cleaned_data
