from django.apps import AppConfig


class GuiprubConfig(AppConfig):
    name = 'guiprub'
