import os
import magic
from django.core.exceptions import ValidationError


def validate_file(file):
    # valid_mime_types = [
    #     '/home/url/Documentos/Trabajo_Terminal/sigesp-project/media/base_prueba']
    # file_mime_type = magic.from_buffer(file.read(1024), mime=True)
    # if file_mime_type not in valid_mime_types:
    #     raise ValidationError('Tipo de archivo no permitido')
    valid_file_extensions = ['.txt', '.tex']
    ext = os.path.splitext(file.name)[1]
    if ext.lower() not in valid_file_extensions:
        raise ValidationError('Extensión de archivo no permitida')


def validate_image(image):
    # valid_mime_types = [
    #     '/home/url/Documentos/Trabajo_Terminal/sigesp-project/media/base_prueba']
    # file_mime_type = magic.from_buffer(file.read(1024), mime=True)
    # if file_mime_type not in valid_mime_types:
    #     raise ValidationError('Tipo de archivo no permitido')
    valid_file_extensions = ['.jpg', '.png', 'jpge']
    ext = os.path.splitext(image.name)[1]
    if ext.lower() not in valid_file_extensions:
        raise ValidationError('Tipo de archivo no permitido')
