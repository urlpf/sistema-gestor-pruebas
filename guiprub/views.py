from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.core.paginator import Paginator
from django.contrib import messages
from .forms import BasePruebaForm, ActualizarGuionForm, RegistrarResultadoForm, RegistrarDefectoForm, ActualizarResultadoForm, ActualizarDefectoForm
from .models import GuionPruebas, BasePrueba, ClaseEquiv, CasoPrueba, Defecto, ResultadoCasoPrueba
from proymod.models import Modulo
import codecs
import pdb
from testsuite.models import TestSuite
from django.utils import timezone
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from proymod.decorators import group_required
import re
from django.db import IntegrityError
from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import MultipleObjectsReturned, ValidationError
from datetime import datetime
import os
from django.http import HttpResponse
from django.templatetags.static import static
import mimetypes
# Create your views here.

mensajes = {
    '001': 'El nombre ingresado ya está registrado en el sistema. Por favor ingresa otro nombre. ',
    '002': 'Elemento registrado exitosamente.',
    '003': 'Elemento modificado exitosamente',
    '004': 'Elemento eliminado exitosamente',
    '005': 'Operación no realizada',
    '006': 'Campos, nombre y tipo de dato son requeridos',
    '007': 'Los valores ingresados son invalidos',
    '008': 'Los valores de limites no pueden ser iguales',
    '009': 'Tipo de dato incorrecto',
    '010': 'Fecha Invalida',
    '011': 'Campo Longitud no ingresado',
    '012': 'La expresion regular es requerida al marcar la opción',
    "013": 'Campos de Limites inferior y Superior son requeridos',
    '014': 'Campo tipo de archivo es requerido',
    '015': 'Campos de fechas mínima y máxima son obligatorios',
    '016': 'Campos requeridos no ingresados',

}
# Indice de guiones


@login_required(login_url="/usuarios/login")
def index_guiones(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    proyecto = modulo.proyecto

    guion_objects = GuionPruebas.objects.filter(modulo=modulo.id)

    # Codigo para buscar por nombre
    guion_name = request.GET.get('item_name')
    if guion_name != '' and guion_name is not None:
        guion_objects = guion_objects.filter(nombre__icontains=guion_name)

    # Codigo para buscar por estado
    guion_estado = request.GET.get('estado')
    if guion_estado != '' and guion_estado is not None:
        guion_objects = guion_objects.filter(estado=guion_estado)

    # Codigo del paginador
    paginator = Paginator(guion_objects, 5)
    page = request.GET.get('page')
    guion_objects = paginator.get_page(page)

    context = {
        'guion_objects': guion_objects,
        'modulo': modulo,
        'proyecto': proyecto,
    }
    return render(request, 'guiprub/index_guiones.html', context)


@login_required(login_url="/usuarios/login")
def detalle_guion(request, guion_id):
    guion = get_object_or_404(GuionPruebas, pk=guion_id)
    ce_objects = ClaseEquiv.objects.filter(guion=guion_id)
    cp_objects = CasoPrueba.objects.filter(guion=guion_id)
    modulo = guion.modulo
    proyecto = modulo.proyecto

    #bad_chars = ['[', ']', "'", "\\", "\"", '[\"', '\']']
    bad_chars = ['[', ']', "\\", '[\"', '\']']
    dict_trayp = dict(guion.trayectoria_p)

    for key, value in dict_trayp.items():
        for i in bad_chars:
            value = value.replace(i, '')
        # dict_trayp[key] = value.split(",|\', \"|\", \'")
        dict_trayp[key] = re.split('\', \"|\", \'|\', \'|\", \"', value)

    for key, value in dict_trayp.items():
        for index, val in enumerate(value):
            value[index] = val.strip()

    dict_trayalt = dict(guion.trayectoria_al)

    for key, value in dict_trayalt.items():
        for i in bad_chars:
            value = value.replace(i, '')
        # dict_trayalt[key] = re.split('\', \"|\", \'', value)
        dict_trayalt[key] = re.split('\', \"|\", \'|\', \'|\", \"', value)

    for key, value in dict_trayalt.items():
        for index, val in enumerate(value):
            value[index] = val.strip()

    ent = list(guion.entradas)
    sal = list(guion.salidas)
    des = list(guion.destinos)

    dict_err = dict(guion.errores)

    for key, value in dict_err.items():
        for i in bad_chars:
            value = value.replace(i, '')
        dict_err[key] = value.split(",")

    for key, value in dict_err.items():
        for index, val in enumerate(value):
            value[index] = val.strip()

    context = {
        'guion': guion,
        'modulo': modulo,
        'dict_trayp': dict_trayp,
        'dict_trayalt': dict_trayalt,
        'dict_err': dict_err,
        'ent': ent,
        'sal': sal,
        'des': des,
        'proyecto': proyecto,
        'ce_objects': ce_objects,
        'cp_objects': cp_objects,

    }
    return render(request, 'guiprub/detalle_guion.html', context)

    # form.save()


@login_required(login_url="/usuarios/login")
def alta_guion(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    form = BasePruebaForm(request.POST or None, request.FILES or None)
    context = {
        'form': form,
        'modulo': modulo
    }
    if request.method == 'POST':
        if form.is_valid():
            try:
                post = form.save(commit=False)
                post.save()
                messages.success(
                    request, "Base de prueba registrada correctamente")
                obj = BasePrueba.objects.get(activated=False)
                return redirect('regex', modulo_id=modulo.id)
            except MultipleObjectsReturned as err:
                base_models = BasePrueba.objects.all()
                for base in base_models:
                    base.activated = True
                    base.save()
                messages.warning(
                    request, "Operacion No realizada")
                return render(request, 'guiprub/alta_guion.html', context)
            except ValidationError as val_err:
                messages.warning(
                    request, "Tipo de archivo no permitido")
                return render(request, 'guiprub/alta_guion.html', context)

        else:
            messages.warning(request, mensajes.get("005"))
    else:
        form = BasePruebaForm()
    return render(request, 'guiprub/alta_guion.html', context)


@login_required(login_url="/usuarios/login")
def baja_base(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    if request.method == 'POST':
        try:
            obj = BasePrueba.objects.get(activated=False)
            obj.delete()
            return redirect('guiones', modulo_id=modulo.id)
        except MultipleObjectsReturned:
            base_models = BasePrueba.objects.all()
            for base in base_models:
                base.activated = True
                base.save()
    return render(request, 'guiprub/alta_guion.html')


@login_required(login_url="/usuarios/login")
def regex(request, modulo_id):
    # Expresiones regulares para buscar dentro del contenido del archivo
    # Titulo
    regex_ti = r"\\UCtitle\{.*\}"
    # Entradas
    regex_ent = r"\\UCRow\{ENTRADAS\}\{.*\}"
    # Salidas
    regex_sal = r"\\UCRow\{SALIDAS.*\}"
    # Destino
    regex_des = r"\\UCRow\{DESTINO\}\{.*\}"
    # Errores
    regex_err = r"\\item\[ERROR\].*"
    # Pasos de Trayectorias
    regex_pasos_trayectorias = r"\\UCStepA\{.*\}"
    # Inicio Trayectoria principal
    regex_tray_p = r"\\UCTrayP"
    # Inicio de Trayectorias alternativas
    regex_tray_alt = r"\\UCTrayA{.*}"
    # End Enumerator
    regex_end = r"\\end{enumerate}"

    modulo = Modulo.objects.get(id=modulo_id)
    obj = BasePrueba.objects.get(activated=False)
    # Codigo para abrir el archivo
    try:
        # with open(obj.base_nombre.path, 'r') as my_file:
        #     text_content = my_file.readlines()
        with codecs.open(obj.base_nombre.path, encoding='utf-8') as my_file:
            pdb.set_trace
            text_content = my_file.readlines()
            for index, x in enumerate(text_content):
                text_content[index] = x.encode('utf-8', 'replace')

        for index, x in enumerate(text_content):
            text_content[index] = x.decode('utf-8', 'replace')
    except FileNotFoundError as fnf_error:
        messages.warning(request, fnf_error)

    else:
        # Convertimos la lista de cadenas obtenida de leer el contenido del archivo a una sola cadena
        cadena_archivo_cont = ' '.join([str(item) for item in text_content])
        # Obtener los objetos con las regex
        resultados_regexp_titulo = re.finditer(
            regex_ti, cadena_archivo_cont, re.MULTILINE)
        resultados_regexp_entrada = re.finditer(
            regex_ent, cadena_archivo_cont, re.MULTILINE)
        resultados_regexp_salidas = re.finditer(
            regex_sal, cadena_archivo_cont, re.MULTILINE)
        resultados_regexp_destino = re.finditer(
            regex_des, cadena_archivo_cont, re.MULTILINE)
        resultados_regexp_error = re.finditer(
            regex_err, cadena_archivo_cont, re.MULTILINE)
        resultados_regexp_tray_p = re.finditer(
            regex_tray_p, cadena_archivo_cont, re.MULTILINE)
        resultados_regexp_tray_alt = re.finditer(
            regex_tray_alt, cadena_archivo_cont, re.MULTILINE)
        resultados_regexp_enu = re.finditer(
            regex_end, cadena_archivo_cont, re.MULTILINE)
        # Listas para guardar los resultados obtenidos y las cadenas encontradas utilizando las expresiones regulares
        titulo = []
        entradas = []
        salidas = []
        errores = []
        destinos = []
        tray_ini_index = []
        tray_alt_title = []
        tray_end_index = []
        trayectoria_principal = []
        resultados_enc = {}
        # Obtencion de cadenas
        # Proceso para obtener el titulo del caso de uso y quitar el codigo en latex del mismo
        for match in resultados_regexp_titulo:
            titulo.append(match.group())
        if len(titulo) == 0:
            # messages.warning(
            #     request, "No se encontro coincidencia con la expresión \\UCtitle{}")
            titulo.append("No encontrado")
            resultados_enc["Titulo"] = titulo
        else:
            for index, item in enumerate(titulo):
                titulo[index] = re.sub(r"\}\{", ":", titulo[index])
                titulo[index] = re.sub(r"\\UCtitle\{|\{|\}", "", titulo[index])
                resultados_enc["Titulo"] = titulo
        # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        # Proceso para obtener las entradas del caso de uso y quitar el codigo en latex del mismo
        for match in resultados_regexp_entrada:
            entradas.append(match.group())
        if len(entradas) == 0:
            # messages.warning(
            #     request, "No se encontro coincidencia con la expresión \\UCRow{ENTRADAS}{}")
            entradas.append("No encontrado")
            resultados_enc["Entradas"] = entradas
        else:
            for index, item in enumerate(entradas):
                entradas[index] = re.sub(
                    r"\\UCRow\{ENTRADAS\}|\{|\}|\\", "", entradas[index])
            entradas = str(entradas)
            bad_chars = ['[', ']', "'"]
            for i in bad_chars:
                entradas = entradas.replace(i, '')
            entradas = entradas.split(",")
            for index, value in enumerate(entradas):
                entradas[index] = value.replace(" ", "")
            resultados_enc["Entradas"] = entradas
        # Proceso para obtener los destinos del caso de uso y quitar el codigo en latex del mismo
        for match in resultados_regexp_destino:
            destinos.append(match.group())
        if len(destinos) == 0:
            print(
                "No se encontro coincidencia con la expresión \\UCRow{DESTINO}{}")
            resultados_enc["Destino"] = "No encontrado"
        else:
            for index, item in enumerate(destinos):
                destinos[index] = re.sub(
                    r"\\UCRow\{DESTINO\}|\[\\ref\{[A-Z0-9]{5,10}:[0-9]{3,9}\}\]|\\ref\{[A-Z0-9]{5,10}:[0-9]{3,9}\}|\\|\{|\}", "", destinos[index])
            destinos = str(destinos)
            bad_chars = ['[', ']', "'"]
            for i in bad_chars:
                destinos = destinos.replace(i, '')
            destinos = destinos.split(",")
            for index, value in enumerate(destinos):
                destinos[index] = re.sub(
                    r' (?=(?:[^"]*"[^"]*")*[^"]*$)', "", value)
            resultados_enc["Destino"] = destinos
        # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        # Proceso para obtener las salidas del caso de uso y quitar el codigo en latex del mismo
        for match in resultados_regexp_salidas:
            salidas.append(match.group())
        if len(salidas) == 0:
            # messages.warning(
            #     request, "No se encontro coincidencia con la expresión \\UCRow{SALIDAS}{}")
            salidas.append("No encontrado")
            resultados_enc["Salidas"] = salidas
        else:
            for index, item in enumerate(salidas):
                salidas[index] = re.sub(
                    r"\\UCRow\{SALIDAS\}|\{|\}|\\", "", salidas[index])
            salidas = str(salidas)
            bad_chars = ['[', ']', "'"]
            for i in bad_chars:
                salidas = salidas.replace(i, '')
            salidas = salidas.split(",")
            for index, value in enumerate(salidas):
                salidas[index] = value.replace(" ", "")
            resultados_enc["Salidas"] = salidas
        # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        # Proceso para obtener los errores del caso de uso y quitar el codigo en latex del mismo
        for match in resultados_regexp_error:
            errores.append(match.group())
        if len(errores) == 0:
            # messages.warning(
            #     request, "No se encontro coincidencia con la expresión \\item[ERROR]{}")
            errores.append("No encontrado")
            err_dic = {
                "Errores": errores,
            }
            resultados_enc["Errores"] = err_dic
        else:
            index_errores = []
            for index, item in enumerate(errores):
                errores[index] = re.sub(
                    r"\\item\[ERROR\]|\{|\}|\\label\{.*\}|\\", "", errores[index])
                index_errores.append("ERROR_" + str(index+1))
            err_dic = dict(zip(index_errores, errores))
            for key_err, val_err in err_dic.items():
                temp_err = err_dic.get(str(key_err))
                err_dic[str(key_err)] = temp_err.split(",")
            resultados_enc["Errores"] = err_dic
        # Agrupar las porciones del codigo que nos entregaran las trayectorias principal y alternativas
        # Primero se obtienen los indices donde se localizan las porciones del texto referentes a las trayectorias principal y alternativas
        for match in resultados_regexp_tray_p:
            tray_ini_index.append(match.start())
        for match in resultados_regexp_tray_alt:
            tray_ini_index.append(match.start())
            tray_alt_title.append(match.group())
        for match in resultados_regexp_enu:
            tray_end_index.append(match.end())

        if len(tray_ini_index) == 0:
            # messages.warning(
            #     request, "No se encontraron coincidencias completas para las trayectorias del CU")
            resultados_enc["Trayectoria principal"] = "No encontrado"
            tray_prin_dic = {
                "Principal": "No enontrado",
            }
            for key, value in tray_prin_dic.items():
                temp_val = tray_prin_dic.get(str(key))
                tray_prin_dic[str(key)] = temp_val.split(",")
        else:
            tray_index_ini_end = list(zip(tray_ini_index, tray_end_index))
            # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            # Obtenemos las porciones de texto siendo la primer instancia encontrada la trayectoria principal y el resto seran las trayectorias alternativas del CU
            conjunto_pasos_trayctoria = []
            for tup in tray_index_ini_end:
                conjunto_pasos_trayctoria.append(
                    cadena_archivo_cont[tup[0]:tup[1]])
            trayectoria_principal_obj = re.finditer(
                regex_pasos_trayectorias, conjunto_pasos_trayctoria[0], re.MULTILINE)
            for match in trayectoria_principal_obj:
                trayectoria_principal.append(match.group())
            # print("1: ", type(trayectoria_principal), trayectoria_principal)

            for index, value in enumerate(trayectoria_principal):
                trayectoria_principal[index] = str(re.sub(
                    r"\\UCStepA\{|\{|\}|\[\\ref\{[A-Z0-9]{5,10}:[0-9]{3,9}\}\]|\\ref\{[A-Z0-9]{5,10}:[0-9]{3,9}\}|\\", "", trayectoria_principal[index]))
            # print("2: ", type(trayectoria_principal), trayectoria_principal)
            joined_tray_p = ",".join(trayectoria_principal)
            # print("3: ", type(joined_tray_p), joined_tray_p)
            # tray_prin_dic = {
            #     "Principal": joined_tray_p,
            # }
            tray_prin_dic = {
                "Principal": trayectoria_principal,
            }
            # for key_dicp, val_dicp in tray_prin_dic.items():
            #     temp_1 = tray_prin_dic.get(str(key_dicp))
            #     print("4: ", type(temp_1), temp_1)
            #     tray_prin_dic[str(key_dicp)] = temp_1.split(",")
            #     print("5: ", type(
            #         tray_prin_dic[str(key_dicp)]), tray_prin_dic[str(key_dicp)])
            resultados_enc["Trayectoria principal"] = tray_prin_dic

            conjunto_pasos_trayctoria.pop(0)
            if len(conjunto_pasos_trayctoria) == 0:
                resultados_enc["Trayectorias alternativas"] = "No encontradas"
                tray_alt_dic = {
                    "Trayectoria Alternativa": "No Encontrada"
                }
                for key, value in tray_alt_dic.items():
                    temp_val = tray_alt_dic.get(str(key))
                    tray_alt_dic[str(key)] = temp_val.split(",")
            else:
                trayectorias_alternativas_obj = []
                for matches in range(len(conjunto_pasos_trayctoria)):
                    trayectorias_alternativas_obj.append(re.finditer(
                        regex_pasos_trayectorias, conjunto_pasos_trayctoria[matches], re.MULTILINE))
                trayectorias_alternativas = [
                    [x.group() for x in l] for l in trayectorias_alternativas_obj]
                for index, item in enumerate(tray_alt_title):
                    tray_alt_title[index] = re.sub(
                        r"\\UCTrayA\{|\}", "", tray_alt_title[index])
                for x in trayectorias_alternativas:
                    for index, value in enumerate(x):
                        x[index] = re.sub(
                            r"\\UCStepA\{|\{|\}|\\|\[\\ref\{[A-Z0-9]{5,10}:[0-9]{3,9}\}\]|\\ref\{[A-Z0-9]{5,10}:[0-9]{3,9}\}", "", x[index])
                # print("2: ", type(trayectorias_alternativas),
                #       trayectorias_alternativas)
                # for ind_x, val_x in enumerate(trayectorias_alternativas):
                #     trayectorias_alternativas[ind_x] = ",".join(
                #         trayectorias_alternativas[ind_x])
                # print("3: ", type(trayectorias_alternativas),
                #       trayectorias_alternativas)
                tray_alt_dic = dict(
                    zip(tray_alt_title, trayectorias_alternativas))
                # for key_dic, val_dic in tray_alt_dic.items():
                #     temp = tray_alt_dic.get(str(key_dic))
                #     tray_alt_dic[str(key_dic)] = temp.split(",")
                # print("4: ", type(tray_alt_dic),
                #       tray_alt_dic)
                resultados_enc["Trayectorias alternativas"] = tray_alt_dic

        if request.method == 'POST':
            try:
                GuionPruebas.objects.create(
                    nombre=titulo[0],
                    trayectoria_p=tray_prin_dic,
                    trayectoria_al=tray_alt_dic,
                    entradas=entradas,
                    destinos=destinos,
                    salidas=salidas,
                    errores=err_dic,
                    modulo=modulo,
                )
            except IntegrityError as err:
                messages.warning(
                    request, "El titulo del CU ya esta registrado en el sistema")
                obj.activated = True
                obj.save()
                obj.delete()
                return redirect('guiones', modulo_id=modulo.id)
            except UnboundLocalError as err_1:
                messages.warning(
                    request, "El archivo ingresado no contiene la estructura para crear un guion de pruebas")
                obj.activated = True
                obj.save()
                obj.delete()
                return redirect('guiones', modulo_id=modulo.id)
            else:
                messages.success(request, mensajes.get("004"))
                obj.activated = True
                obj.save()
                obj.delete()
                return redirect('guiones', modulo_id=modulo.id)

        context = {
            'text_content': resultados_enc,
            'modulo': modulo,
            'titulo': titulo,
            'tray_prin_dic': tray_prin_dic,
            'tray_alt_dic': tray_alt_dic,
            'entradas': entradas,
            'destinos': destinos,
            'salidas': salidas,
            'err_dic': err_dic
        }
    return render(request, 'guiprub/regex.html', context)


@login_required(login_url="/usuarios/login")
def borrar_guion(request, guion_id):
    guion = get_object_or_404(GuionPruebas, pk=guion_id)
    modulo = guion.modulo

    if request.method == 'POST':
        guion.delete()
        messages.success(request, mensajes.get("004"))
        return redirect('guiones', modulo_id=modulo.id)

    context = {
        'modulo': modulo,
        'guion': guion,
    }
    return render(request, 'guiprub/borrar_guion.html', context)


@login_required(login_url="/usuarios/login")
def actualizar_estado(request, guion_id):
    guion = get_object_or_404(GuionPruebas, pk=guion_id)
    cp_objects = CasoPrueba.objects.filter(guion=guion)

    cp_results = []
    aprobado = 0
    no_aprobado = 0
    sin_probar = 0
    impedimentos = 0
    for cp in cp_objects:
        cp_results.append(cp.resultado)

    for resultado in cp_results:
        if resultado == "Aprobado":
            aprobado = aprobado + 1
        elif resultado == "No Aprobado":
            no_aprobado = no_aprobado + 1
        elif resultado == "Sin Probar":
            sin_probar = sin_probar + 1
        elif resultado == "Con Impedimentos":
            impedimentos = impedimentos + 1
    if ("No Aprobado" in cp_results) or ("Sin Probar" in cp_results) or ("Con Impedimentos" in cp_results):
        guion.estado = "No liberado"
        guion.save()
        edo = "No liberado"
    elif not("No Aprobado" in cp_results) and not("Sin Probar" in cp_results) and not("Con Impedimentos" in cp_results):
        guion.estado = "Liberado"
        guion.save()
        edo = "Liberado"
    messages.info(
        request, f"Estado de guion actualizado a '{edo}'. Pruebas aprobadas {aprobado}, pruebas no aprobadas {no_aprobado}, pruebas sin probar {sin_probar}, pruebas con impedimentos {impedimentos}")
    return redirect('detalleg', guion_id=guion.id)


@login_required(login_url="/usuarios/login")
def actualizar_guion(request, guion_id):
    guion = get_object_or_404(GuionPruebas, pk=guion_id)
    modulo = guion.modulo
    print("guion pk", guion.pk)
    if request.method == 'POST':
        form = ActualizarGuionForm(request.POST or None, instance=guion)
        guion_update = form.instance
        print("Object_updated pk", guion_update.pk)
        if form.is_valid():
            form.save()
            messages.success(request, mensajes.get("003"))
            return redirect('detalleg', guion_id=guion.id)
        else:
            messages.warning(
                request, mensajes.get("005"))
    else:
        form = ActualizarGuionForm(instance=guion)

    context = {
        'form': form,
        'guion': guion,
        'modulo': modulo
    }
    return render(request, 'guiprub/actualizar_guion.html', context)


@login_required(login_url="/usuarios/login")
def alta_ce(request, guion_id):
    guion = GuionPruebas.objects.get(id=guion_id)
    modulo = guion.modulo
    entradas = enumerate(list(guion.entradas))
    entrys = list(guion.entradas)
    context = {
        "guion": guion,
        "modulo": modulo,
        "entradas": entradas,
        "entrys": entrys,
    }
    if request.method == 'POST':
        try:
            name = str(request.POST['name'])
            type_input = str(request.POST['type_input'])
        except MultiValueDictKeyError:
            type_input = False
            name = False
        if name and type_input:
            if type_input == "ENTERO":
                try:
                    limite_inf_int = int(request.POST.get('limite_inf_int'))
                    limite_sup_int = int(request.POST.get('limite_sup_int'))
                except ValueError:
                    limite_inf_int = False
                    limite_sup_int = False
                if limite_inf_int and limite_sup_int:
                    if limite_inf_int > limite_sup_int:
                        temp = limite_sup_int
                        limite_sup_int = limite_inf_int
                        limite_inf_int = temp
                    elif limite_inf_int == limite_sup_int:
                        messages.warning(
                            request, mensajes.get("008"))
                        return render(request, 'guiprub/alta_ce.html', context)
                    condiciones = {}
                    condiciones[f'X es un número entero menor a {limite_inf_int}'] = "No Valido"
                    condiciones[f'X es un número entero mayor a {limite_sup_int}'] = "No Valido"
                    condiciones[f'X es un número entero mayor a {limite_inf_int} y menor a {limite_sup_int}'] = "Valido"
                    try:
                        limites_int = int(request.POST.get('limites_int'))
                    except ValueError:
                        limites_int = False
                    if limites_int == 1:
                        condiciones[f'X es un número entero igual a {limite_inf_int}'] = "Valido"
                        condiciones[f'X es un número entero igual a {limite_sup_int}'] = "Valido"
                    else:
                        condiciones[f'X es un número entero igual a {limite_inf_int}'] = "No Valido"
                        condiciones[f'X es un número entero igual a {limite_sup_int}'] = "No Valido"
                    try:
                        cero_int = int(request.POST.get('cero_int'))
                    except ValueError:
                        cero_int = False
                    if cero_int == 1:
                        condiciones[f'X es igual a 0'] = "Valido"
                    else:
                        condiciones[f'X es igual a 0'] = "No Valido"
                    try:
                        obli_data = int(request.POST.get('obli_data'))
                    except ValueError:
                        obli_data = False
                    if obli_data == 1:
                        condiciones[f'X esta vacio'] = "No Valido"
                    else:
                        condiciones[f'X esta vacio'] = "Valido"
                    condiciones[f'X no es un número entero '] = "No Valido"
                    marklist = sorted(
                        ((value, key) for (key, value) in condiciones.items()), reverse=True)
                    sortdict = dict([(k, v) for v, k in marklist])
                    try:
                        ce = ClaseEquiv()
                        ce.nombre = request.POST['name']
                        ce.tipo_dato = request.POST['type_input']
                        ce.condiciones = sortdict
                        ce.guion = guion
                        ce.save()
                        messages.success(
                            request, mensajes.get("002"))
                        return redirect('detalleg', guion_id=guion.id)
                    except IntegrityError as err:
                        messages.warning(
                            request, "El nombre de la Clase de Equivalencia ya esta registrado en el sistema")
                        return render(request, 'guiprub/alta_ce.html', context)
                else:
                    messages.warning(request, mensajes.get("013"))
                    return render(request, 'guiprub/alta_ce.html', context)

            elif type_input == "FLOTANTE":
                try:
                    limite_inf_float = float(
                        request.POST.get('limite_inf_float'))
                    limite_sup_float = float(
                        request.POST.get('limite_sup_float'))
                except ValueError:
                    limite_inf_float = False
                    limite_sup_float = False
                if limite_inf_float and limite_sup_float:
                    if limite_inf_float > limite_sup_float:
                        temp = limite_inf_float
                        limite_sup_float = limite_inf_float
                        limite_inf_float = temp
                    elif limite_inf_float == limite_sup_float:
                        messages.warning(
                            request, mensajes.get("008"))
                        return render(request, 'guiprub/alta_ce.html', context)
                    condiciones = {}
                    condiciones[f'X es un número flotante menor a {limite_inf_float}'] = "No Valido"
                    condiciones[f'X es un número flotante mayor a {limite_sup_float}'] = "No Valido"
                    condiciones[f'X es un número flotante mayor a {limite_inf_float} y menor a {limite_sup_float}'] = "Valido"
                    try:
                        limites_float = int(request.POST.get('limites_float'))
                    except ValueError:
                        limites_float = False
                    if limites_float == 1:
                        condiciones[f'X es un número flotante igual a {limite_inf_float}'] = "Valido"
                        condiciones[f'X es un número flotante igual a {limite_sup_float}'] = "Valido"
                    else:
                        condiciones[f'X es un número flotante igual a {limite_inf_float}'] = "No Valido"
                        condiciones[f'X es un número flotante igual a {limite_sup_float}'] = "No Valido"
                    try:
                        cero_float = int(request.POST.get('cero_float'))
                    except ValueError:
                        cero_float = False
                    if cero_float == 1:
                        condiciones[f'X es igual a 0'] = "Valido"
                    else:
                        condiciones[f'X es igual a 0'] = "No Valido"
                    try:
                        obli_data = int(request.POST.get('obli_data'))
                    except ValueError:
                        obli_data = False
                    if obli_data == 1:
                        condiciones[f'x esta vacio'] = "No Valido"
                    else:
                        condiciones[f'X esta vacio'] = "Valido"
                    condiciones[f'X no es un número'] = "No Valido"
                    marklist = sorted(
                        ((value, key) for (key, value) in condiciones.items()), reverse=True)
                    sortdict = dict([(k, v) for v, k in marklist])
                    try:
                        ce = ClaseEquiv()
                        ce.nombre = request.POST['name']
                        ce.tipo_dato = request.POST['type_input']
                        ce.condiciones = sortdict
                        ce.guion = guion
                        ce.save()
                        messages.success(
                            request, mensajes.get("002"))
                        return redirect('detalleg', guion_id=guion.id)
                    except IntegrityError as err:
                        messages.warning(
                            request, "El nombre de la Clase de Equivalencia ya esta registrado en el sistema")
                        return render(request, 'guiprub/alta_ce.html', context)
                else:
                    messages.warning(request, mensajes.get("013"))
                    return render(request, 'guiprub/alta_ce.html', context)
            elif type_input == "CADENA":
                try:
                    longitud = int(request.POST.get('longitud'))
                except ValueError:
                    longitud = False
                if longitud:
                    try:
                        longfija = int(request.POST['longfija'])
                    except ValueError:
                        longfija = False
                    try:
                        regexp = int(request.POST['regexp'])
                    except ValueError:
                        regexp = False
                    try:
                        alfanumerico = int(request.POST['alfanumerico'])
                    except ValueError:
                        alfanumerico = False
                    try:
                        car_especial = int(request.POST['especial'])
                    except ValueError:
                        car_especial = False
                    condiciones = {}
                    if (alfanumerico == 1) and (car_especial == 1):
                        condiciones['X es una cadena alfanumerica con caracteres especiales'] = "Valido"
                        condiciones['X no es una cadena alfanumerica con caracteres especiales'] = "No Valido"
                    elif (alfanumerico == 1) and not(car_especial == 1):
                        condiciones['X es una cadena alfanumerica'] = "Valido"
                        condiciones['X no es una cadena alfanumerica'] = "No Valido"
                    elif not(alfanumerico == 1) and (car_especial == 1):
                        condiciones['X es una cadena alfanumerica'] = "Valido"
                        condiciones['X no es una cadena alfanumerica'] = "No Valido"
                    elif not(alfanumerico == 1) and not(car_especial == 1):
                        condiciones['X es una cadena alfabetica'] = "Valido"
                        condiciones['X no es una cadena alfabetica'] = "No Valido"
                    itemsList = condiciones.items()
                    for key, value in itemsList:
                        if value == "Valido":
                            key_todrop = key
                    if longfija == 1:
                        condiciones[f'{key_todrop} con longitud igual a {longitud}'] = "Valido"
                        condiciones[f'{key_todrop} con longitud diferente de {longitud}'] = "No Valido"
                    else:
                        condiciones[f'{key_todrop} con longitud menor a {longitud}'] = "Valido"
                        condiciones[f'{key_todrop} con longitud mayor a {longitud}'] = "No Valido"
                    condiciones.pop(key_todrop)
                    itemsList = condiciones.items()
                    for key, value in itemsList:
                        if value == "Valido":
                            key_todrop = key
                    if regexp == 1:
                        try:
                            expreg = str(request.POST['expreg'])
                        except ValueError:
                            expreg = False
                        if expreg:
                            condiciones[f'{key_todrop}. La cadena tiene estructura con la expresión regular {expreg}'] = "Valido"
                            condiciones[f'{key_todrop}. La cadena no tiene estructura con la expresión regular {expreg}'] = "No Valido"
                        else:
                            messages.warning(request, mensajes.get("012"))
                            return render(request, 'guiprub/alta_ce.html', context)
                        condiciones.pop(key_todrop)
                    else:
                        pass
                    itemsList = condiciones.items()
                    try:
                        obli_data = int(request.POST.get('obli_data'))
                    except ValueError:
                        obli_data = False
                    if obli_data == 1:
                        condiciones[f'X esta vacio'] = "No Valido"
                    else:
                        condiciones[f'X esta vacio'] = "Valido"
                    marklist = sorted(
                        ((value, key) for (key, value) in condiciones.items()), reverse=True)
                    sortdict = dict([(k, v) for v, k in marklist])
                    try:
                        ce = ClaseEquiv()
                        ce.nombre = request.POST['name']
                        ce.tipo_dato = request.POST['type_input']
                        ce.condiciones = sortdict
                        ce.guion = guion
                        ce.save()
                        messages.success(
                            request, mensajes.get("002"))
                        return redirect('detalleg', guion_id=guion.id)
                    except IntegrityError as err:
                        messages.warning(
                            request, "El nombre de la Clase de Equivalencia ya esta registrado en el sistema")
                        return render(request, 'guiprub/alta_ce.html', context)
                else:
                    messages.warning(request, mensajes.get("010"))
                    return render(request, 'guiprub/alta_ce.html', context)
            elif type_input == "LISTA":
                condiciones = {}
                condiciones['X es un elemento de la lista'] = "Valido"
                condiciones['X no es un elemento de la lista'] = "No Valido"
                try:
                    obli_data = int(request.POST.get('obli_data'))
                except ValueError:
                    obli_data = False
                if obli_data == 1:
                    condiciones[f'X esta vacio'] = "No Valido"
                else:
                    condiciones[f'X esta vacio'] = "Valido"
                marklist = sorted(
                    ((value, key) for (key, value) in condiciones.items()), reverse=True)
                sortdict = dict([(k, v) for v, k in marklist])
                try:
                    ce = ClaseEquiv()
                    ce.nombre = request.POST['name']
                    ce.tipo_dato = request.POST['type_input']
                    ce.condiciones = sortdict
                    ce.guion = guion
                    ce.save()
                    messages.success(
                        request, mensajes.get("002"))
                    return redirect('detalleg', guion_id=guion.id)
                except IntegrityError as err:
                    messages.warning(
                        request, "El nombre de la Clase de Equivalencia ya esta registrado en el sistema")
                    return render(request, 'guiprub/alta_ce.html', context)
            elif type_input == "FECHA":
                try:
                    fecha_min = request.POST['fecha_min']
                    fecha_max = request.POST['fecha_max']
                except ValueError:
                    fecha_min = False
                    fecha_max = False
                if request.POST['fecha_min'] and request.POST['fecha_max']:
                    try:
                        fecha_min = datetime.strftime(
                            datetime.strptime(fecha_min, '%Y-%m-%d'), '%Y/%m/%d')
                        fecha_max = datetime.strftime(
                            datetime.strptime(fecha_max, '%Y-%m-%d'), '%Y/%m/%d')
                        yearmin, monthmin, daymin = fecha_min.split('/')
                        yearmax, monthmax, daymax = fecha_max.split('/')
                        isValidDate = True
                        try:
                            datetime(int(yearmin), int(monthmin), int(daymin))
                            datetime(int(yearmax), int(monthmax), int(daymax))
                        except ValueError:
                            isValidDate = False
                    except ValueError:
                        isValidDate = False
                    if isValidDate:
                        if fecha_min > fecha_max:
                            temp = fecha_max
                            fecha_max = fecha_min
                            fecha_min = temp
                        elif fecha_min == fecha_max:
                            messages.warning(
                                request, mensajes.get("008"))
                            return render(request, 'guiprub/alta_ce.html', context)
                        condiciones = {}
                        condiciones[f'X es una fecha menor a {fecha_min}'] = "No Valido"
                        condiciones[f'X es una fecha mayor a {fecha_max}'] = "No Valido"
                        condiciones[f'X es una fecha mayor a {fecha_min} y menor a {fecha_max}'] = "Valido"
                        try:
                            obli_data = int(request.POST.get('obli_data'))
                        except ValueError:
                            obli_data = False
                        if obli_data == 1:
                            condiciones[f'X esta vacio'] = "No Valido"
                        else:
                            condiciones[f'X esta vacio'] = "Valido"
                        condiciones[f'X no es una fecha'] = "No Valido"
                        marklist = sorted(
                            ((value, key) for (key, value) in condiciones.items()), reverse=True)
                        sortdict = dict([(k, v) for v, k in marklist])
                        try:
                            ce = ClaseEquiv()
                            ce.nombre = request.POST['name']
                            ce.tipo_dato = request.POST['type_input']
                            ce.condiciones = sortdict
                            ce.guion = guion
                            ce.save()
                            messages.success(
                                request, mensajes.get("002"))
                            return redirect('detalleg', guion_id=guion.id)
                        except IntegrityError as err:
                            messages.warning(
                                request, "El nombre de la Clase de Equivalencia ya esta registrado en el sistema")
                            return render(request, 'guiprub/alta_ce.html', context)
                    else:
                        messages.warning(request, mensajes.get("015"))
                        return render(request, 'guiprub/alta_ce.html', context)
                else:
                    messages.warning(request, mensajes.get("010"))
                    return render(request, 'guiprub/alta_ce.html', context)
            elif type_input == "ARCHIVO":
                try:
                    type_file = str(request.POST['type_file'])
                except MultiValueDictKeyError:
                    type_file = False
                if type_file:
                    try:
                        tamano = float(request.POST['tamano'])
                        longitud = int(request.POST['longitud_nombre'])
                    except ValueError:
                        tamano = False
                        longitud = False
                    if tamano and longitud:
                        condiciones = {}
                        condiciones[f'X es un archivo {type_file} con nombre con longitud menor a {longitud} y con tamaño menor a {tamano}mb'] = "Valido"
                        condiciones[f'X es un archivo {type_file} con nombre con longitud mayor a {longitud} y con tamaño menor a {tamano}mb'] = "No Valido"
                        condiciones[f'X es un archivo {type_file} con nombre con longitud menor a {longitud} y con tamaño mayor a {tamano}mb'] = "No Valido"
                        condiciones[f'X no es un archivo {type_file}'] = "No Valido"
                        try:
                            obli_data = int(request.POST.get('obli_data'))
                        except ValueError:
                            obli_data = False
                        if obli_data == 1:
                            condiciones[f'X esta vacio'] = "No Valido"
                        else:
                            condiciones[f'X esta vacio'] = "Valido"
                        marklist = sorted(
                            ((value, key) for (key, value) in condiciones.items()), reverse=True)
                        sortdict = dict([(k, v) for v, k in marklist])
                        try:
                            ce = ClaseEquiv()
                            ce.nombre = request.POST['name']
                            ce.tipo_dato = request.POST['type_input']
                            ce.condiciones = sortdict
                            ce.guion = guion
                            ce.save()
                            messages.success(
                                request, mensajes.get("002"))
                            return redirect('detalleg', guion_id=guion.id)
                        except IntegrityError as err:
                            messages.warning(
                                request, "El nombre de la Clase de Equivalencia ya esta registrado en el sistema")
                            return render(request, 'guiprub/alta_ce.html', context)
                else:
                    messages.warning(request, mensajes.get("014"))
                    return render(request, 'guiprub/alta_ce.html', context)
            else:
                messages.warning(request, mensajes.get("006"))
                return render(request, 'guiprub/alta_ce.html', context)
        else:
            messages.warning(request, mensajes.get("006"))
            return render(request, 'guiprub/alta_ce.html', context)
    else:
        return render(request, 'guiprub/alta_ce.html', context)

    return render(request, 'guiprub/alta_ce.html', context)


@login_required(login_url="/usuarios/login")
def detalle_ce(request, ce_id):
    claseequivalencia = get_object_or_404(ClaseEquiv, pk=ce_id)
    guion = claseequivalencia.guion
    modulo = guion.modulo
    proyecto = modulo.proyecto

    condiciones_dic = dict(claseequivalencia.condiciones)
    condiciones_items = condiciones_dic.items()
    context = {
        "claseequivalencia": claseequivalencia,
        "condiciones_items": condiciones_items,
        "guion": guion,
        "modulo": modulo,
        "proyecto": proyecto,
    }
    return render(request, 'guiprub/detalle_ce.html', context)


@login_required(login_url="/usuarios/login")
def actualizar_ce(request, ce_id):
    claseequivalencia = get_object_or_404(ClaseEquiv, pk=ce_id)
    guion = claseequivalencia.guion
    modulo = guion.modulo
    entrys = list(guion.entradas)
    context = {
        "claseequivalencia": claseequivalencia,
        "modulo": modulo,
        "entrys": entrys,
    }
    if request.method == 'POST':
        try:
            type_input = request.POST['type_input']
        except MultiValueDictKeyError:
            type_input = False
        if type_input:
            if request.POST['type_input'] == "ENTERO":
                try:
                    limite_inf_int = int(request.POST.get('limite_inf_int'))
                    limite_sup_int = int(request.POST.get('limite_sup_int'))
                except ValueError:
                    limite_inf_int = False
                    limite_sup_int = False
                if limite_inf_int and limite_sup_int:
                    if limite_inf_int > limite_sup_int:
                        temp = limite_sup_int
                        limite_sup_int = limite_inf_int
                        limite_inf_int = temp
                    elif limite_inf_int == limite_sup_int:
                        messages.warning(
                            request, mensajes.get("008"))
                        return render(request, 'guiprub/actualizar_ce.html', context)
                    condiciones = {}
                    condiciones[f'X es un número entero menor a {limite_inf_int}'] = "No Valido"
                    condiciones[f'X es un número entero mayor a {limite_sup_int}'] = "No Valido"
                    condiciones[f'X es un número entero mayor a {limite_inf_int} y menor a {limite_sup_int}'] = "Valido"
                    try:
                        limites_int = int(request.POST.get('limites_int'))
                    except ValueError:
                        limites_int = False
                    if limites_int == 1:
                        condiciones[f'X es un número entero igual a {limite_inf_int}'] = "Valido"
                        condiciones[f'X es un número entero igual a {limite_sup_int}'] = "Valido"
                    else:
                        condiciones[f'X es un número entero igual a {limite_inf_int}'] = "No Valido"
                        condiciones[f'X es un número entero igual a {limite_sup_int}'] = "No Valido"
                    try:
                        cero_int = int(request.POST.get('cero_int'))
                    except ValueError:
                        cero_int = False
                    if cero_int == 1:
                        condiciones[f'X es igual a 0'] = "Valido"
                    else:
                        condiciones[f'X es igual a 0'] = "No Valido"
                    try:
                        obli_data = int(request.POST.get('obli_data'))
                    except ValueError:
                        obli_data = False
                    if obli_data == 1:
                        condiciones[f'X esta vacio'] = "No Valido"
                    else:
                        condiciones[f'X esta vacio'] = "Valido"
                    condiciones[f'X no es un número entero '] = "No Valido"
                    marklist = sorted(
                        ((value, key) for (key, value) in condiciones.items()), reverse=True)
                    sortdict = dict([(k, v) for v, k in marklist])

                    claseequivalencia.condiciones = sortdict
                    claseequivalencia.tipo_dato = request.POST['type_input']
                    claseequivalencia.save()
                    messages.success(request, mensajes.get("002"))
                    return redirect('detallece', ce_id=claseequivalencia.id)
                else:
                    messages.warning(request, mensajes.get("013"))
                    return render(request, 'guiprub/actualizar_ce.html', context)
            elif request.POST['type_input'] == "FLOTANTE":
                try:
                    limite_inf_float = float(
                        request.POST.get('limite_inf_float'))
                    limite_sup_float = float(
                        request.POST.get('limite_sup_float'))
                except ValueError:
                    limite_inf_float = False
                    limite_sup_float = False
                if limite_inf_float and limite_sup_float:
                    if limite_inf_float > limite_sup_float:
                        temp = limite_inf_float
                        limite_sup_float = limite_inf_float
                        limite_inf_float = temp
                    elif limite_inf_float == limite_sup_float:
                        messages.warning(
                            request, mensajes.get("008"))
                        return render(request, 'guiprub/actualizar_ce.html', context)
                    condiciones = {}
                    condiciones[f'X es un número flotante menor a {limite_inf_float}'] = "No Valido"
                    condiciones[f'X es un número flotante mayor a {limite_sup_float}'] = "No Valido"
                    condiciones[f'X es un número flotante mayor a {limite_inf_float} y menor a {limite_sup_float}'] = "Valido"
                    try:
                        limites_float = int(request.POST.get('limites_float'))
                    except ValueError:
                        limites_float = False
                    if limites_float == 1:
                        condiciones[f'X es un número flotante igual a {limite_inf_float}'] = "Valido"
                        condiciones[f'X es un número flotante igual a {limite_sup_float}'] = "Valido"
                    else:
                        condiciones[f'X es un número flotante igual a {limite_inf_float}'] = "No Valido"
                        condiciones[f'X es un número flotante igual a {limite_sup_float}'] = "No Valido"
                    try:
                        cero_float = int(request.POST.get('cero_float'))
                    except ValueError:
                        cero_float = False
                    if cero_float == 1:
                        condiciones[f'X es igual a 0'] = "Valido"
                    else:
                        condiciones[f'X es igual a 0'] = "No Valido"
                    try:
                        obli_data = int(request.POST.get('obli_data'))
                    except ValueError:
                        obli_data = False
                    if obli_data == 1:
                        condiciones[f'X esta vacio'] = "No Valido"
                    else:
                        condiciones[f'X esta vacio'] = "Valido"
                    condiciones[f'X no es un número'] = "No Valido"
                    marklist = sorted(
                        ((value, key) for (key, value) in condiciones.items()), reverse=True)
                    sortdict = dict([(k, v) for v, k in marklist])
                    claseequivalencia.condiciones = sortdict
                    claseequivalencia.tipo_dato = request.POST['type_input']
                    claseequivalencia.save()
                    messages.success(request, mensajes.get("002"))
                    return redirect('detallece', ce_id=claseequivalencia.id)
                else:
                    messages.warning(request, mensajes.get("013"))
                    return render(request, 'guiprub/actualizar_ce.html', context)
            elif request.POST['type_input'] == "CADENA":
                try:
                    longitud = int(request.POST.get('longitud'))
                except ValueError:
                    longitud = False
                if longitud:
                    try:
                        longfija = int(request.POST['longfija'])
                    except ValueError:
                        longfija = False
                    try:
                        regexp = int(request.POST['regexp'])
                    except ValueError:
                        regexp = False
                    try:
                        alfanumerico = int(request.POST['alfanumerico'])
                    except ValueError:
                        alfanumerico = False
                    try:
                        car_especial = int(request.POST['especial'])
                    except ValueError:
                        car_especial = False
                    condiciones = {}
                    if (alfanumerico == 1) and (car_especial == 1):
                        condiciones['X es una cadena alfanumerica con caracteres especiales'] = "Valido"
                        condiciones['X no es una cadena alfanumerica con caracteres especiales'] = "No Valido"
                    elif (alfanumerico == 1) and not(car_especial == 1):
                        condiciones['X es una cadena alfanumerica'] = "Valido"
                        condiciones['X no es una cadena alfanumerica '] = "No Valido"
                    elif not(alfanumerico == 1) and (car_especial == 1):
                        condiciones['X es una cadena alfanumerica'] = "Valido"
                        condiciones['X no es una cadena alfanumerica '] = "No Valido"
                    elif not(alfanumerico == 1) and not(car_especial == 1):
                        condiciones['X es una cadena alfabetica'] = "Valido"
                        condiciones['X no es una cadena alfabetica '] = "No Valido"
                    itemsList = condiciones.items()
                    for key, value in itemsList:
                        if value == "Valido":
                            key_todrop = key
                    if longfija == 1:
                        condiciones[f'{key_todrop} con longitud igual a {longitud}'] = "Valido"
                        condiciones[f'{key_todrop} con longitud diferente de {longitud}'] = "No Valido"
                    else:
                        condiciones[f'{key_todrop} con longitud menor a {longitud}'] = "Valido"
                        condiciones[f'{key_todrop} con longitud mayor a {longitud}'] = "No Valido"
                    condiciones.pop(key_todrop)
                    itemsList = condiciones.items()
                    for key, value in itemsList:
                        if value == "Valido":
                            key_todrop = key
                    if regexp == 1:
                        try:
                            expreg = str(request.POST['expreg'])
                        except ValueError:
                            expreg = False
                        if expreg:
                            condiciones[f'{key_todrop}. La cadena tiene estructura con la expresión regular {expreg}'] = "Valido"
                            condiciones[f'{key_todrop}. La cadena no tiene estructura con la expresión regular {expreg}'] = "No Valido"
                        else:
                            messages.warning(request, mensajes.get("012"))
                            return render(request, 'guiprub/actualizar_ce.html', context)
                        condiciones.pop(key_todrop)
                    else:
                        pass
                    itemsList = condiciones.items()
                    try:
                        obli_data = int(request.POST.get('obli_data'))
                    except ValueError:
                        obli_data = False
                    if obli_data == 1:
                        condiciones[f'X esta vacio'] = "No Valido"
                    else:
                        condiciones[f'X esta vacio'] = "Valido"
                    marklist = sorted(
                        ((value, key) for (key, value) in condiciones.items()), reverse=True)
                    sortdict = dict([(k, v) for v, k in marklist])
                    claseequivalencia.condiciones = sortdict
                    claseequivalencia.tipo_dato = request.POST['type_input']
                    claseequivalencia.save()
                    messages.success(request, mensajes.get("002"))
                    return redirect('detallece', ce_id=claseequivalencia.id)
                else:
                    messages.warning(request, mensajes.get("010"))
                    return render(request, 'guiprub/actualizar_ce.html', context)
            elif type_input == "LISTA":
                condiciones = {}
                condiciones['X es un elemento de la lista'] = "Valido"
                condiciones['X no es un elemento de la lista'] = "No Valido"
                try:
                    obli_data = int(request.POST.get('obli_data'))
                except ValueError:
                    obli_data = False
                if obli_data == 1:
                    condiciones[f'X esta vacio'] = "No Valido"
                else:
                    condiciones[f'X esta vacio'] = "Valido"
                marklist = sorted(
                    ((value, key) for (key, value) in condiciones.items()), reverse=True)
                sortdict = dict([(k, v) for v, k in marklist])
                claseequivalencia.condiciones = sortdict
                claseequivalencia.tipo_dato = request.POST['type_input']
                claseequivalencia.save()
                messages.success(request, mensajes.get("002"))
                return redirect('detallece', ce_id=claseequivalencia.id)
            elif request.POST['type_input'] == "FECHA":
                try:
                    fecha_min = request.POST['fecha_min']
                    fecha_max = request.POST['fecha_max']
                except ValueError:
                    fecha_min = False
                    fecha_max = False
                if request.POST['fecha_min'] and request.POST['fecha_max']:
                    try:
                        fecha_min = datetime.strftime(
                            datetime.strptime(fecha_min, '%Y-%m-%d'), '%Y/%m/%d')
                        fecha_max = datetime.strftime(
                            datetime.strptime(fecha_max, '%Y-%m-%d'), '%Y/%m/%d')
                        yearmin, monthmin, daymin = fecha_min.split('/')
                        yearmax, monthmax, daymax = fecha_max.split('/')
                        isValidDate = True
                        try:
                            datetime(int(yearmin), int(monthmin), int(daymin))
                            datetime(int(yearmax), int(monthmax), int(daymax))
                        except ValueError:
                            isValidDate = False
                    except ValueError:
                        isValidDate = False
                    if isValidDate:
                        if fecha_min > fecha_max:
                            temp = fecha_max
                            fecha_max = fecha_min
                            fecha_min = temp
                        elif fecha_min == fecha_max:
                            messages.warning(
                                request, mensajes.get("008"))
                            return render(request, 'guiprub/alta_ce.html', context)
                        condiciones = {}
                        condiciones[f'X es una fecha menor a {fecha_min}'] = "No Valido"
                        condiciones[f'X es una fecha mayor a {fecha_max}'] = "No Valido"
                        condiciones[f'X es una fecha mayor a {fecha_min} y menor a {fecha_max}'] = "Valido"
                        try:
                            obli_data = int(request.POST.get('obli_data'))
                        except ValueError:
                            obli_data = False
                        if obli_data == 1:
                            condiciones[f'X esta vacio'] = "No Valido"
                        else:
                            condiciones[f'X esta vacio'] = "Valido"
                        condiciones[f'X no es una fecha'] = "No Valido"
                        marklist = sorted(
                            ((value, key) for (key, value) in condiciones.items()), reverse=True)
                        sortdict = dict([(k, v) for v, k in marklist])
                        claseequivalencia.condiciones = sortdict
                        claseequivalencia.tipo_dato = request.POST['type_input']
                        claseequivalencia.save()
                        messages.success(request, mensajes.get("002"))
                        return redirect('detallece', ce_id=claseequivalencia.id)
                    else:
                        messages.warning(request, mensajes.get("015"))
                        return render(request, 'guiprub/alta_ce.html', context)
                else:
                    messages.warning(request, mensajes.get("010"))
                    return render(request, 'guiprub/alta_ce.html', context)
            elif request.POST['type_input'] == "ARCHIVO":
                try:
                    type_file = str(request.POST['type_file'])
                except MultiValueDictKeyError:
                    type_file = False
                if type_file:
                    try:
                        tamano = float(request.POST['tamano'])
                        longitud = int(request.POST['longitud_nombre'])
                    except ValueError:
                        tamano = False
                        longitud = False
                    if tamano and longitud:
                        condiciones = {}
                        condiciones[f'X es un archivo {type_file} con nombre con longitud menor a {longitud} y con tamaño menor a {tamano}mb'] = "Valido"
                        condiciones[f'X es un archivo {type_file} con nombre con longitud mayor a {longitud} y con tamaño menor a {tamano}mb'] = "No Valido"
                        condiciones[f'X es un archivo {type_file} con nombre con longitud menor a {longitud} y con tamaño mayor a {tamano}mb'] = "No Valido"
                        condiciones[f'X no es un archivo {type_file}'] = "No Valido"
                        try:
                            obli_data = int(request.POST.get('obli_data'))
                        except ValueError:
                            obli_data = False
                        if obli_data == 1:
                            condiciones[f'X esta vacio'] = "No Valido"
                        else:
                            condiciones[f'X esta vacio'] = "Valido"
                        marklist = sorted(
                            ((value, key) for (key, value) in condiciones.items()), reverse=True)
                        sortdict = dict([(k, v) for v, k in marklist])
                        claseequivalencia.condiciones = sortdict
                        claseequivalencia.tipo_dato = request.POST['type_input']
                        claseequivalencia.save()
                        messages.success(request, mensajes.get("002"))
                        return redirect('detallece', ce_id=claseequivalencia.id)
                    else:
                        messages.warning(request, mensajes.get("006"))
                        return render(request, 'guiprub/actualizar_ce.html', context)
                else:
                    messages.warning(request, mensajes.get("014"))
                    return render(request, 'guiprub/actualizar_ce.html', context)
        else:
            messages.warning(request, mensajes.get("006"))
            return render(request, 'guiprub/actualizar_ce.html', context)
    else:
        return render(request, 'guiprub/actualizar_ce.html', context)

    return render(request, 'guiprub/actualizar_ce.html', context)


@login_required(login_url="/usuarios/login")
def borrar_ce(request, ce_id):
    claseequivalencia = get_object_or_404(ClaseEquiv, pk=ce_id)
    guion = claseequivalencia.guion

    if request.method == 'POST':
        claseequivalencia.delete()
        messages.success(request, mensajes.get("004"))
        return redirect('detalleg', guion_id=guion.id)

    context = {
        'claseequivalencia': claseequivalencia,
        'guion': guion,
    }
    return render(request, 'guiprub/borrar_ce.html', context)


@login_required(login_url="/usuarios/login")
def alta_cp(request, guion_id):
    guion = get_object_or_404(GuionPruebas, pk=guion_id)
    ce_objects = ClaseEquiv.objects.filter(guion=guion_id)
    if not ce_objects:
        messages.warning(
            request, "No hay Clases de equivalencia para armar un guion de pruebas")
        return redirect('detalleg', guion_id=guion.id)
    else:
        tray_principal = dict(guion.trayectoria_p)
        tray_alt = dict(guion.trayectoria_al)
        trayectorias = []
        ce_dic = {}
        bad_chars = ['[', ']', "\\", '[\"', '\']']
        #"No encontradas"
        for ce in ce_objects:
            ce_dic[str(ce.nombre)] = dict(ce.condiciones)

        for key, value in tray_alt.items():
            for i in bad_chars:
                value = value.replace(i, '')
            tray_alt[key] = re.split('\', \"|\", \'', value)

        for key, value in tray_alt.items():
            for index, val in enumerate(value):
                value[index] = val.strip()

        for key, value in tray_alt.items():
            print(len(value))
            if "No Encontrada" in value:
                print(value)
            # if "No Encontrada" in value and len(value) == 1:
            #     pass
            # else:
            #     trayectorias.append(str(key))

        context = {
            'guion': guion,
            'ce_objects': ce_objects,
            'ce_dic': ce_dic,
            'trayectorias': trayectorias,
        }
        if request.method == 'POST':
            try:
                ce_nombre = [(str(n), str(v)) for n, v in request.POST.items()
                             if n.startswith('CENombre')]
                ce_valor = [(str(n), str(v)) for n, v in request.POST.items()
                            if n.startswith('CEValor')]
                for index, ce in enumerate(ce_nombre):
                    ce_nombre[index] = ce[1]
                for index, ce in enumerate(ce_valor):
                    ce_valor[index] = ce[1]
                entradas_dic = dict(zip(ce_nombre, ce_valor))

                list_salida = [(str(n), str(v)) for n, v in request.   POST.items()
                               if n.startswith('salida')]
                for index, value in enumerate(list_salida):
                    list_salida[index] = value[1]
                list_salida = set(list_salida)
                list_salida = list(list_salida)

                for index, value in enumerate(list_salida):
                    if value == "Otra":
                        list_salida.pop(index)
                        if len(list_salida) == 0:
                            list_salida.append("Ninguna")
                        else:
                            pass
                    else:
                        pass

                if "Ninguna" in list_salida:
                    list_salida = ["Ninguna"]

                trayectoria = str(request.POST['trayectoria'])
                titulo = str(request.POST['titulo'])
                # salida = str(request.POST['salida'])
                destino = str(request.POST['destino'])
            except ValueError:
                ce_nombre = False
                ce_valor = False
                titulo = False
                entradas_dic = False
                trayectoria = False
                # salida = False
                list_salida = False
                destino = False

            if ce_nombre and ce_valor and titulo and list_salida and destino and trayectoria and entradas_dic:
                descripcion = {}
                if trayectoria == "SoloPrincipal":
                    descripcion = tray_principal.copy()
                else:
                    for key, value in guion.trayectoria_al.items():
                        if trayectoria == key:
                            descripcion = tray_principal.copy()
                            descripcion[str(key)] = value

                try:
                    CasoPrueba.objects.create(
                        titulo=titulo,
                        descripcion=descripcion,
                        entradas=entradas_dic,
                        resultados_esp=list_salida,
                        destino=destino,
                        guion=guion,
                    )
                except IntegrityError as err:
                    messages.warning(
                        request, "El titulo del CP ya esta registrado en el sistema")
                    return redirect('detalleg', guion_id=guion.id)
                else:
                    messages.success(request, mensajes.get("002"))
                    return redirect('detalleg', guion_id=guion.id)
            else:
                messages.warning(request, mensajes.get("016"))
                return render(request, 'guiprub/alta_cp.html', context)
        else:
            return render(request, 'guiprub/alta_cp.html', context)
    return render(request, 'guiprub/alta_cp.html', context)


@login_required(login_url="/usuarios/login")
def detalle_cp(request, cp_id):
    casoprueba = get_object_or_404(CasoPrueba, pk=cp_id)
    resultado_objects = ResultadoCasoPrueba.objects.filter(
        caso_prueba=casoprueba)
    guion = casoprueba.guion
    modulo = guion.modulo
    suite = guion.suite
    proyecto = modulo.proyecto

    resultado = str(casoprueba.resultado)

    # bad_chars = ['[', ']', "'", "\\", "\""]
    dict_desc = dict(casoprueba.descripcion)

    bad_chars = ['[', ']', "\\", '[\"', '\']']
    # dict_trayp = dict(guion.trayectoria_p)

    for key, value in dict_desc.items():
        for i in bad_chars:
            value = value.replace(i, '')
        # dict_trayp[key] = value.split(",|\', \"|\", \'")
        dict_desc[key] = re.split('\', \"|\", \'', value)

    # for key, value in dict_desc.items():
    #     for i in bad_chars:
    #         value = value.replace(i, '')
    #     dict_desc[key] = value.split(",")

    for key, value in dict_desc.items():
        for index, val in enumerate(value):
            value[index] = val.strip()

    list_salidas = list(casoprueba.resultados_esp)
    dict_entradas = dict(casoprueba.entradas)

    for key, value in dict_entradas.items():
        for i in bad_chars:
            value = value.replace(i, '')
        dict_entradas[key] = value.split(",")

    for key, value in dict_entradas.items():
        for index, val in enumerate(value):
            value[index] = val.strip()

    context = {
        "casoprueba": casoprueba,
        "guion": guion,
        "modulo": modulo,
        "proyecto": proyecto,
        "dict_desc": dict_desc,
        "list_salidas": list_salidas,
        "dict_entradas": dict_entradas,
        "resultado": resultado,
        "resultado_objects": resultado_objects,
        "suite": suite,
    }
    return render(request, 'guiprub/detalle_cp.html', context)


@login_required(login_url="/usuarios/login")
def actualizar_cp(request, cp_id):
    casoprueba = get_object_or_404(CasoPrueba, pk=cp_id)
    guion = casoprueba.guion
    ce_objects = ClaseEquiv.objects.filter(guion=guion.id)
    caso_trayectorias = dict(casoprueba.descripcion)
    caso_entradas = dict(casoprueba.entradas)
    caso_salidas = list(casoprueba.resultados_esp)

    if not ce_objects:
        messages.warning(
            request, "No hay Clases de equivalencia para armar un guion de pruebas")
        return redirect('detalleg', guion_id=guion.id)
    else:
        if len(caso_trayectorias.keys()) == 1:
            caso_trayectoria = "SoloPrincipal"
        else:
            for x in caso_trayectorias.keys():
                if x != "Principal":
                    caso_trayectoria = x
                else:
                    pass
        tray_principal = dict(guion.trayectoria_p)
        tray_alt = dict(guion.trayectoria_al)
        print(tray_alt)
        trayectorias = []
        ce_dic = {}
        for ce in ce_objects:
            ce_dic[str(ce.nombre)] = dict(ce.condiciones)

        for key, value in ce_dic.items():
            list_temp = []
            for inner_key, inner_value in value.items():
                list_temp.append(f"{inner_key}--({inner_value})")
            ce_dic[key] = list_temp

        for key, value in tray_alt.items():
            if "No Encontrada" in value and len(value) == 1:
                pass
            else:
                trayectorias.append(str(key))

        search_list = []
        for key, value in caso_entradas.items():
            search_list.append(value)

        context = {
            'guion': guion,
            'ce_dic': ce_dic,
            'trayectorias': trayectorias,
            'casoprueba': casoprueba,
            'caso_trayectoria': caso_trayectoria,
            'caso_entradas': caso_entradas,
            'search_list': search_list,
            'caso_salidas': caso_salidas,
        }
        if request.method == 'POST':
            try:
                ce_nombre = [(str(n), str(v)) for n, v in request.POST.items()
                             if n.startswith('CENombre')]
                ce_valor = [(str(n), str(v)) for n, v in request.POST.items()
                            if n.startswith('CEValor')]
                for index, ce in enumerate(ce_nombre):
                    ce_nombre[index] = ce[1]
                for index, ce in enumerate(ce_valor):
                    ce_valor[index] = ce[1]
                entradas_dic = dict(zip(ce_nombre, ce_valor))
                trayectoria = str(request.POST['trayectoria'])
                titulo = str(request.POST['titulo'])

                list_salida = [(str(n), str(v)) for n, v in request.   POST.items()
                               if n.startswith('salida')]
                for index, value in enumerate(list_salida):
                    list_salida[index] = value[1]
                list_salida = set(list_salida)
                list_salida = list(list_salida)

                for index, value in enumerate(list_salida):
                    if value == "Otra":
                        list_salida.pop(index)
                    else:
                        pass

                caso_salidas.extend(list_salida)
                caso_salidas = set(caso_salidas)
                caso_salidas = list(caso_salidas)

                list_salida_rem = [(str(n), str(v)) for n, v in request.   POST.items()
                                   if n.startswith('remsal')]
                for index, value in enumerate(list_salida_rem):
                    list_salida_rem[index] = value[1]
                list_salida_rem = set(list_salida_rem)
                list_salida_rem = list(list_salida_rem)

                if len(list_salida_rem) > 0:
                    for index, value in enumerate(caso_salidas):
                        if value in list_salida_rem:
                            caso_salidas.pop(index)
                            if len(caso_salidas) == 0:
                                caso_salidas = ["Ninguna"]
                                break
                        else:
                            continue
                else:
                    pass

                if "Ninguna" in list_salida:
                    list_salida = ["Ninguna"]

                # salida = str(request.POST['salida'])
                destino = str(request.POST['destino'])
            except ValueError:
                ce_nombre = False
                ce_valor = False
                titulo = False
                entradas_dic = False
                trayectoria = False
                list_salida = False
                destino = False

            if ce_nombre and ce_valor and titulo and destino and trayectoria and entradas_dic:
                descripcion = {}
                if trayectoria == "SoloPrincipal":
                    descripcion = tray_principal.copy()
                else:
                    for key, value in guion.trayectoria_al.items():
                        if trayectoria == key:
                            descripcion = tray_principal.copy()
                            descripcion[str(key)] = value

                try:
                    casoprueba.titulo = titulo
                    casoprueba.descripcion = descripcion
                    casoprueba.entradas = entradas_dic
                    casoprueba.resultados_esp = caso_salidas
                    casoprueba.destino = destino
                    casoprueba.save()
                except IntegrityError as err:
                    messages.warning(
                        request, "El titulo del CP ya esta registrado en el sistema")
                    return redirect('detallecp', cp_id=casoprueba.id)
                else:
                    messages.success(request, mensajes.get("003"))
                    return redirect('detallecp', cp_id=casoprueba.id)
            else:
                messages.warning(request, mensajes.get("016"))
                return render(request, 'guiprub/actualizar_cp.html', context)
    return render(request, 'guiprub/actualizar_cp.html', context)


@login_required(login_url="/usuarios/login")
def borrar_cp(request, cp_id):
    casoprueba = get_object_or_404(CasoPrueba, pk=cp_id)
    guion = casoprueba.guion

    if request.method == 'POST':
        casoprueba.delete()
        messages.success(request, mensajes.get("004"))
        return redirect('detalleg', guion_id=guion.id)

    context = {
        'casoprueba': casoprueba,
        'guion': guion,
    }
    return render(request, 'guiprub/borrar_cp.html', context)


@login_required(login_url="/usuarios/login")
def registrar_resultado(request, cp_id):
    casoprueba = get_object_or_404(CasoPrueba, pk=cp_id)
    dict_desc = dict(casoprueba.descripcion)
    bad_chars = ['[', ']', "\\", '[\"', '\']']
    for key, value in dict_desc.items():
        for i in bad_chars:
            value = value.replace(i, '')
        dict_desc[key] = re.split('\', \"|\", \'', value)

    for key, value in dict_desc.items():
        for index, val in enumerate(value):
            value[index] = val.strip()

    list_salidas = list(casoprueba.resultados_esp)
    dict_entradas = dict(casoprueba.entradas)

    for key, value in dict_entradas.items():
        for i in bad_chars:
            value = value.replace(i, '')
        dict_entradas[key] = value.split(",")

    for key, value in dict_entradas.items():
        for index, val in enumerate(value):
            value[index] = val.strip()

    if request.method == 'POST':
        form = RegistrarResultadoForm(
            request.POST or None)
        if form.is_valid():
            post = form.save(commit=False)
            post.caso_prueba = casoprueba
            post.save()
            resul_prueba = ResultadoCasoPrueba.objects.filter(
                caso_prueba=casoprueba).latest('fecha')
            casoprueba.resultado = resul_prueba.resultado
            casoprueba.fecha_ult = resul_prueba.fecha
            casoprueba.save()
            messages.success(request, mensajes.get("003"))
            return redirect('detallecp', cp_id=casoprueba.id)
        else:
            messages.warning(request, mensajes.get("005"))
    else:
        form = RegistrarResultadoForm()
    context = {
        'form': form,
        'casoprueba': casoprueba,
        "dict_desc": dict_desc,
        "list_salidas": list_salidas,
        "dict_entradas": dict_entradas,
    }
    return render(request, 'guiprub/alta_resultado.html', context)


@login_required(login_url="/usuarios/login")
def detalle_resultado(request, resultado_id):
    resul_prueba = get_object_or_404(ResultadoCasoPrueba, pk=resultado_id)
    casoprueba = resul_prueba.caso_prueba
    defecto_objects = Defecto.objects.filter(resultado=resul_prueba)
    guion = casoprueba.guion
    modulo = guion.modulo
    proyecto = modulo.proyecto

    context = {
        "resul_prueba": resul_prueba,
        "casoprueba": casoprueba,
        "defecto_objects": defecto_objects,
        "guion": guion,
        "modulo": modulo,
        "proyecto": proyecto,
    }
    return render(request, 'guiprub/detalle_resultado.html', context)


@login_required(login_url="/usuarios/login")
def actualizar_resultado(request, resultado_id):
    resul_prueba = get_object_or_404(ResultadoCasoPrueba, pk=resultado_id)
    casoprueba = resul_prueba.caso_prueba
    dict_desc = dict(casoprueba.descripcion)
    bad_chars = ['[', ']', "\\", '[\"', '\']']
    for key, value in dict_desc.items():
        for i in bad_chars:
            value = value.replace(i, '')
        dict_desc[key] = re.split('\', \"|\", \'', value)

    for key, value in dict_desc.items():
        for index, val in enumerate(value):
            value[index] = val.strip()

    list_salidas = list(casoprueba.resultados_esp)
    dict_entradas = dict(casoprueba.entradas)

    for key, value in dict_entradas.items():
        for i in bad_chars:
            value = value.replace(i, '')
        dict_entradas[key] = value.split(",")

    for key, value in dict_entradas.items():
        for index, val in enumerate(value):
            value[index] = val.strip()
    if request.method == 'POST':
        form = ActualizarResultadoForm(
            request.POST or None, instance=resul_prueba)
        if form.is_valid():
            form.save()
            resultado_prueba = ResultadoCasoPrueba.objects.filter(
                caso_prueba=casoprueba).latest('fecha')
            casoprueba.resultado = resultado_prueba.resultado
            casoprueba.fecha_ult = resultado_prueba.fecha
            casoprueba.save()
            messages.success(request, mensajes.get("003"))
            return redirect('detalle_resultado', resultado_id=resul_prueba.id)
        else:
            messages.warning(request, mensajes.get("005"))
    else:
        form = ActualizarResultadoForm(instance=resul_prueba)
    context = {
        'form': form,
        'casoprueba': casoprueba,
        'resul_prueba': resul_prueba,
        "dict_desc": dict_desc,
        "list_salidas": list_salidas,
        "dict_entradas": dict_entradas,
    }
    return render(request, 'guiprub/actualizar_resultado.html', context)


@login_required(login_url="/usuarios/login")
def borrar_resultado(request, resultado_id):
    resul_prueba = get_object_or_404(ResultadoCasoPrueba, pk=resultado_id)
    casoprueba = resul_prueba.caso_prueba

    if request.method == 'POST':
        resul_prueba.delete()
        messages.success(request, mensajes.get("004"))
        return redirect('detallecp', cp_id=casoprueba.id)

    context = {
        'resul_prueba': resul_prueba,
        'casoprueba': casoprueba,
    }
    return render(request, 'guiprub/borrar_resultado.html', context)


@login_required(login_url="/usuarios/login")
def altadefecto(request, resultado_id):
    resultado = get_object_or_404(ResultadoCasoPrueba, pk=resultado_id)
    if request.method == 'POST':
        form = RegistrarDefectoForm(request.POST or None, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.titulo = form.cleaned_data['titulo']
            post.resultado = resultado
            post.save()
            messages.success(request, mensajes.get("002"))
            return redirect('detalle_resultado', resultado_id=resultado.id)
        else:
            messages.warning(
                request, mensajes.get("005"))
    else:
        form = RegistrarDefectoForm()
    context = {
        'form': form,
        'resultado': resultado,
    }
    return render(request, 'guiprub/alta_defecto.html', context)

# @login_required(login_url="/usuarios/login")
# def altadefecto(request, cp_id):
#     casoprueba = get_object_or_404(CasoPrueba, pk=cp_id)
#     context = {
#         'casoprueba': casoprueba,
#     }
#     if request.method == 'POST':
#         try:
#             titulo = str(request.POST['titulo'])
#             tipo_defecto = str(request.POST['tipo_defecto'])
#             prioridad = str(request.POST['prioridad'])
#             descripcion = str(request.POST['descripcion'])
#         except ValueError:
#             titulo = False
#             tipo_defecto = False
#             prioridad = False
#             descripcion = False
#         if titulo and tipo_defecto and prioridad and descripcion:
#             try:
#                 tevidencia = int(request.POST['tevidencia'])
#             except ValueError:
#                 tevidencia = False
#             if tevidencia == 1:
#                 try:
#                     Defecto.objects.create(
#                         titulo=titulo,
#                         descripcion=descripcion,
#                         tipo_defecto=tipo_defecto,
#                         prioridad=prioridad,
#                         evidencia=request.FILES['evidencia'],
#                         casoprueba=casoprueba,
#                     )
#                 except IntegrityError as err:
#                     messages.warning(
#                         request, "El titulo del defecto ya esta registrado en el sistema")
#                     return redirect('detallecp', cp_id=casoprueba.id)
#                 except ValidationError as val_err:
#                     messages.warning(request, "Tipo de archivo no permitido")
#                     return redirect('detallecp', cp_id=casoprueba.id)
#                 else:
#                     messages.success(request, mensajes.get("002"))
#                     return redirect('detallecp', cp_id=casoprueba.id)
#             else:
#                 try:
#                     Defecto.objects.create(
#                         titulo=titulo,
#                         descripcion=descripcion,
#                         tipo_defecto=tipo_defecto,
#                         prioridad=prioridad,
#                         casoprueba=casoprueba,
#                     )
#                 except IntegrityError as err:
#                     messages.warning(
#                         request, "El titulo del defecto ya esta registrado en el sistema")
#                     return redirect('detallecp', cp_id=casoprueba.id)
#                 else:
#                     messages.success(request, mensajes.get("002"))
#                     return redirect('detallecp', cp_id=casoprueba.id)
#         else:
#             messages.warning(
#                 request, mensajes.get("005"))

#     else:
#         return render(request, 'guiprub/alta_defecto.html', context)

#     return render(request, 'guiprub/alta_defecto.html', context)


@login_required(login_url="/usuarios/login")
def detalledefecto(request, defecto_id):
    defecto = get_object_or_404(Defecto, pk=defecto_id)
    resultado = defecto.resultado
    casoprueba = resultado.caso_prueba
    guion = casoprueba.guion
    modulo = guion.modulo
    proyecto = modulo.proyecto

    context = {
        "defecto": defecto,
        "resultado": resultado,
        "casoprueba": casoprueba,
        "guion": guion,
        "modulo": modulo,
        "proyecto": proyecto,
    }
    return render(request, 'guiprub/detalle_defecto.html', context)


@login_required(login_url="/usuarios/login")
def actualizar_defecto(request, defecto_id):
    defecto = Defecto.objects.get(id=defecto_id)
    resultado = defecto.resultado
    if request.method == 'POST':
        form = ActualizarDefectoForm(request.POST or None, instance=defecto)
        if form.is_valid():
            form.save()
            messages.success(request, mensajes.get("003"))
            return redirect('detalledefecto', defecto_id=defecto.id)
        else:
            messages.warning(request, mensajes.get("005"))
    else:
        form = ActualizarDefectoForm(instance=defecto)
    context = {
        'form': form,
        'defecto': defecto,
        'resultado': resultado,
    }

    return render(request, 'guiprub/actualizar_defecto.html', context)


@login_required(login_url="/usuarios/login")
def borrar_defecto(request, defecto_id):
    defecto = get_object_or_404(Defecto, pk=defecto_id)
    resultado = defecto.resultado

    if request.method == 'POST':
        defecto.delete()
        messages.success(request, mensajes.get("004"))
        return redirect('detalle_resultado', resultado_id=resultado.id)

    context = {
        'defecto': defecto,
        'resultado': resultado,
    }
    return render(request, 'guiprub/borrar_defecto.html', context)


@login_required(login_url="/usuarios/login")
def prueba(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    dic_1 = {'breed': 'labrador'}
    dic_2 = {'breed': 'labrador', 'dato2': ['dat_1', 'dat_2']}
    list_1 = ['dat_1', 'dat_2']
    if request.method == 'POST':
        GuionPruebas.objects.create(
            nombre='titulo_2',
            trayectoria_p=dic_1,
            trayectoria_al=dic_2,
            entradas=list_1,
            salidas=list_1,
            errores=dic_2,
            modulo=modulo
        )
        messages.success(request, mensajes.get("004"))
        return redirect('guiones', modulo_id=modulo.id)

    return render(request, 'guiprub/prueba.html', {'modulo': modulo})
# PRUEBA


# @login_required(login_url="/usuarios/login")
# def descargar_plantilla(request):
#     filepath = os.path.join('static', 'sigesp/Plantilla_Latex.tex')
#     with open(filepath, 'rb') as tex:
#         print(type(tex))
#         # text_content = tex.readlines()
#         # print(type(text_content))
#         response = HttpResponse(
#             tex.read().decode('utf-8', 'replace'),
#             content_type='text/plain')
#         response['Content-Disposition'] = 'inline;filename=Plantilla_Latex.tex'
#     return response


@login_required(login_url="/usuarios/login")
def descargar_plantilla(request):
    filepath = os.path.join('static', 'sigesp/Plantilla_Latex.tex')
    try:
        with codecs.open(filepath, encoding='utf-8') as my_file:
            pdb.set_trace
            text_content = my_file.readlines()
            for index, x in enumerate(text_content):
                text_content[index] = x.encode('utf-8', 'replace')

        for index, x in enumerate(text_content):
            text_content[index] = x.decode('utf-8', 'replace')
    except FileNotFoundError as fnf_error:
        messages.warning(request, fnf_error)
    else:
        response = HttpResponse(
            text_content,
            content_type='text/plain')
        # response['Content-Disposition'] = 'inline;filename=Plantilla_Latex.tex'
        response['Content-Disposition'] = 'attachment; filename="Plantilla_Latex.tex"'
    return response
