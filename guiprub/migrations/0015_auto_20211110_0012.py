# Generated by Django 3.1.7 on 2021-11-10 05:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('guiprub', '0014_guionpruebas_suite'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='casoprueba',
            name='fecha_prueba',
        ),
        migrations.RemoveField(
            model_name='casoprueba',
            name='observaciones',
        ),
        migrations.AddField(
            model_name='guionpruebas',
            name='estado',
            field=models.CharField(choices=[('Liberado', 'Liberado'), ('No liberado', 'No liberado')], default='No liberado', max_length=20),
        ),
        migrations.AlterField(
            model_name='casoprueba',
            name='resultado',
            field=models.CharField(choices=[('Aprobado', 'Aprobado'), ('No Aprobado', 'No Aprobado'), ('Sin Probar', 'Sin Probar'), ('Con Impedimentos', 'Con Impedimentos')], default='Sin Probar', max_length=50),
        ),
        migrations.CreateModel(
            name='ResultadoCasoPrueba',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('resultado', models.CharField(choices=[('Aprobado', 'Aprobado'), ('No Aprobado', 'No Aprobado'), ('Sin Probar', 'Sin Probar'), ('Con Impedimentos', 'Con Impedimentos')], default='Sin Probar', max_length=50)),
                ('observaciones', models.TextField(blank=True, max_length=200, null=True)),
                ('caso_prueba', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='guiprub.casoprueba')),
            ],
        ),
        migrations.AddField(
            model_name='defecto',
            name='resultado',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='guiprub.resultadocasoprueba'),
        ),
        migrations.AlterUniqueTogether(
            name='defecto',
            unique_together={('titulo', 'resultado')},
        ),
        migrations.RemoveField(
            model_name='defecto',
            name='casoprueba',
        ),
    ]
