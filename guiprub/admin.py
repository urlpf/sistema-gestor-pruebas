from django.contrib import admin
from .models import GuionPruebas, BasePrueba, ClaseEquiv, CasoPrueba, Defecto
from django import forms
from django_admin_hstore_widget.forms import HStoreFormField


class GuionPruebasAdminForm(forms.ModelForm):
    trayectoria_p = HStoreFormField()
    trayectoria_al = HStoreFormField()
    errores = HStoreFormField()

    class Meta:
        model = GuionPruebas
        exclude = ()


@admin.register(GuionPruebas)
class GuionPruebasAdmin(admin.ModelAdmin):
    form = GuionPruebasAdminForm


# Register your models here.
# admin.site.register(GuionPruebas)
admin.site.register(BasePrueba)
admin.site.register(Defecto)


class CaseEquivAdminForm(forms.ModelForm):
    condiciones = HStoreFormField()

    class Meta:
        model = ClaseEquiv
        exclude = ()


@admin.register(ClaseEquiv)
class ClaseEquivAdmin(admin.ModelAdmin):
    form = CaseEquivAdminForm


class CasoPruebaAdminForm(forms.ModelForm):
    descripcion = HStoreFormField()
    entradas = HStoreFormField()

    class Meta:
        model = CasoPrueba
        exclude = ()


@admin.register(CasoPrueba)
class CasoPruebaAdmin(admin.ModelAdmin):
    form = CasoPruebaAdminForm
