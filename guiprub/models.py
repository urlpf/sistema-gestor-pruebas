from django.contrib.postgres.fields import ArrayField, HStoreField
from django.db import models
from proymod.models import Modulo
from testsuite.models import TestSuite
from .validators import validate_file, validate_image
from datetime import date

# Create your models here.
ESTADO_GUION = [
    ('Liberado', 'Liberado'),
    ('No liberado', 'No liberado'),
]


class GuionPruebas(models.Model):
    nombre = models.CharField(max_length=150)
    trayectoria_p = HStoreField(blank=True)
    trayectoria_al = HStoreField(blank=True)
    entradas = ArrayField(models.CharField(max_length=200), blank=True)
    destinos = ArrayField(models.CharField(max_length=200), blank=True)
    salidas = ArrayField(models.CharField(max_length=200), blank=True)
    errores = HStoreField(blank=True)
    estado = models.CharField(
        max_length=20, default="No liberado", choices=ESTADO_GUION)
    modulo = models.ForeignKey(Modulo, on_delete=models.CASCADE)
    suite = models.ForeignKey(TestSuite, blank=True,
                              null=True, on_delete=models.SET_NULL)

    class Meta:
        unique_together = ["nombre", "modulo"]

    def __str__(self):
        return self.nombre


class BasePrueba(models.Model):
    base_nombre = models.FileField(
        upload_to='base_prueba/', max_length=50, validators=(validate_file,))
    activated = models.BooleanField(default=False)

    def __str__(self):
        return f"File id: {self.id}"

    def delete(self, *args, **kwargs):
        self.base_nombre.delete()
        super().delete(*args, **kwargs)


class ClaseEquiv(models.Model):
    nombre = models.CharField(max_length=150)
    tipo_dato = models.CharField(max_length=150)
    condiciones = HStoreField(blank=True)
    guion = models.ForeignKey(GuionPruebas, on_delete=models.CASCADE)

    class Meta:
        unique_together = ["nombre", "guion"]

    def __str__(self):
        return self.nombre


RESULTADOS_PRUEBAS = [
    ('Aprobado', 'Aprobado'),
    ('No Aprobado', 'No Aprobado'),
    ('Sin Probar', 'Sin Probar'),
    ('Con Impedimentos', 'Con Impedimentos')
]


class CasoPrueba(models.Model):
    titulo = models.CharField(max_length=200)
    descripcion = HStoreField()
    entradas = HStoreField()
    # resultados_esp = models.CharField(max_length=30)
    resultados_esp = ArrayField(models.CharField(
        max_length=50), blank=True, null=True)
    destino = models.CharField(max_length=300)
    resultado = models.CharField(
        max_length=50, default="Sin Probar", choices=RESULTADOS_PRUEBAS)
    fecha_ult = models.DateField(auto_now=False, null=True, default=date.today)
    guion = models.ForeignKey(GuionPruebas, on_delete=models.CASCADE)

    class Meta:
        unique_together = ["titulo", "guion"]

    def __str__(self):
        return self.titulo


class ResultadoCasoPrueba(models.Model):
    fecha = models.DateField(auto_now=False)
    resultado = models.CharField(
        max_length=50, default="Sin Probar", choices=RESULTADOS_PRUEBAS)
    observaciones = models.TextField(max_length=200, blank=True, null=True)
    caso_prueba = models.ForeignKey(CasoPrueba, on_delete=models.CASCADE)

    def __str__(self):
        return f"Resultado: {self.fecha}"


ESTADOS_DEFECTO = [
    ('No Notificado', 'No Notificado'),
    ('Notificado', 'Notificado'),
    ('Replicado', 'Replicado'),
    ('En Liberacion', 'En Liberacion'),
    ('Liberado por QA', 'Liberado por QA'),
]

TIPO_DEFECTO = [
    ('Visual', 'Visual'),
    ('Funcional', 'Funcional'),
    ('Problemas de rendimiento', 'Problemas de rendimiento'),
]

PRIORIDAD = [
    ('Alta', 'Alta'),
    ('Media', 'Media'),
    ('Baja', 'Baja'),
]


# class Defecto(models.Model):
#     titulo = models.CharField(max_length=200)
#     estado = models.CharField(
#         max_length=50, default="No Notificado", choices=ESTADOS_DEFECTO)
#     tipo_defecto = models.CharField(
#         max_length=50,  choices=TIPO_DEFECTO)
#     prioridad = models.CharField(
#         max_length=50,  choices=PRIORIDAD)
#     descripcion = models.TextField(max_length=300)
#     evidencia = models.ImageField(
#         upload_to='evidencias/', blank=True, null=True, validators=(validate_image,))
#     casoprueba = models.ForeignKey(CasoPrueba, on_delete=models.CASCADE)

#     def __str__(self):
#         return self.titulo

#     def delete(self, *args, **kwargs):
#         self.evidencia.delete()
#         super().delete(*args, **kwargs)

#     class Meta:
#         unique_together = ["titulo", "casoprueba"]

class Defecto(models.Model):
    titulo = models.CharField(max_length=200)
    estado = models.CharField(
        max_length=50, default="No Notificado", choices=ESTADOS_DEFECTO)
    tipo_defecto = models.CharField(
        max_length=50,  choices=TIPO_DEFECTO)
    prioridad = models.CharField(
        max_length=50,  choices=PRIORIDAD)
    descripcion = models.TextField(max_length=300)
    evidencia = models.ImageField(
        upload_to='evidencias/', blank=True, null=True, validators=(validate_image,))
    resultado = models.ForeignKey(
        ResultadoCasoPrueba, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.titulo

    def delete(self, *args, **kwargs):
        self.evidencia.delete()
        super().delete(*args, **kwargs)

    class Meta:
        unique_together = ["titulo", "resultado"]
