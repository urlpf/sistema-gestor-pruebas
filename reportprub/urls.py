from django.urls import path
from . import views

urlpatterns = [
    path('reporte_avance/<int:modulo_id>/', views.reporte_avance, name='reporte_avance'),
    path('reporte_def/<int:modulo_id>/', views.reporte_def, name='reporte_def'),
]
