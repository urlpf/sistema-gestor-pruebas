from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.contrib import messages
from guiprub.models import GuionPruebas, BasePrueba, ClaseEquiv, CasoPrueba, Defecto, ResultadoCasoPrueba
from proymod.models import Modulo
from testsuite.models import TestSuite
from django.utils import timezone
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from proymod.decorators import group_required
import re
from django.db import IntegrityError
from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import MultipleObjectsReturned, ValidationError
from django.http import HttpResponse
from django.db.models import Count
from django.http import FileResponse
import io
# from reportlab.lib.units import inch
from reportlab.lib.pagesizes import letter, A4
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, PageBreak
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import Table
from reportlab.platypus import TableStyle
from reportlab.lib import colors
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.shapes import String
from reportlab.platypus import Image
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.graphics.charts.piecharts import (
    Pie
)
# from reportlab.graphics.charts.legends import (
#     Legend
# )
# from reportlab.lib.validators import Auto


@login_required(login_url="/usuarios/login")
@group_required('TestManager')
def reporte_avance(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    proyecto = modulo.proyecto
    guion_objects = GuionPruebas.objects.filter(modulo=modulo)
    guion_objects_total = guion_objects.count()
    guion_objects_lib = guion_objects.filter(estado="Liberado").count()
    guion_objects_notlib = guion_objects.filter(estado="No liberado").count()
    cp_objects = CasoPrueba.objects.filter(guion__in=guion_objects)
    cp_objects_total = CasoPrueba.objects.filter(
        guion__in=guion_objects).count()
    aprobado = cp_objects.filter(
        guion__in=guion_objects, resultado="Aprobado").count()
    no_aprobado = cp_objects.filter(
        guion__in=guion_objects, resultado="No Aprobado").count()
    sin_probar = cp_objects.filter(
        guion__in=guion_objects, resultado="Sin Probar").count()
    impedimentos = cp_objects.filter(
        guion__in=guion_objects, resultado="Con Impedimentos").count()

    context = {
        'modulo': modulo,
        'proyecto': proyecto,
        'guion_objects': guion_objects,
        'cp_objects': cp_objects,
    }

    # response = HttpResponse()
    # response.write("<p>Total guiones</p>")
    # response.write(guion_objects_total)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total guiones liberados</p>")
    # response.write(guion_objects_lib)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total guiones no liberados</p>")
    # response.write(guion_objects_notlib)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total pruebas</p>")
    # response.write(cp_objects_total)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total pruebas: Aprobado</p>")
    # response.write(aprobado)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total pruebas: No Aprobado</p>")
    # response.write(no_aprobado)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total pruebas: Sin Probar</p>")
    # response.write(sin_probar)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total pruebas: Con Impedimentos</p>")
    # response.write(impedimentos)
    # response.write("<p>--------------------<p>")

    result_objects = ResultadoCasoPrueba.objects.filter(
        caso_prueba__in=cp_objects)
    defect_objects = Defecto.objects.filter(
        resultado__in=result_objects).exclude(estado="Liberado por QA")
    defect_objects_total = defect_objects.count()
    prior_alta = defect_objects.filter(
        resultado__in=result_objects, prioridad="Alta").count()
    prior_media = defect_objects.filter(
        resultado__in=result_objects, prioridad="Media").count()
    prior_baja = defect_objects.filter(
        resultado__in=result_objects, prioridad="Baja").count()

    # response.write("<p>Total defectos activos</p>")
    # response.write(defect_objects_total)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total defectos prioridad alta</p>")
    # response.write(prior_alta)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total defectos prioridad media</p>")
    # response.write(prior_media)
    # response.write("<p>--------------------<p>")
    # response.write("<p>Total defectos prioridad baja</p>")
    # response.write(prior_baja)

    data_guiones = [
        ['Nombre de Proyecto', proyecto.nombre],
        ['Nombre de módulo', modulo.nombre],
        ['Guiones de prueba totales', guion_objects_total],
        ['Guiones liberados', guion_objects_lib],
        ['Guiones no liberados', guion_objects_notlib],
        ['Pruebas totales', cp_objects_total],
        ['Pruebas aprobadas', aprobado],
        ['Pruebas no aprobadas', no_aprobado],
        ['Pruebas con impedimentos', impedimentos],
        ['Pruebas sin probar', sin_probar],
    ]

    # Create PDF
    # Head and titles
    nombre_archivo = "Reporte"+modulo.nombre
    titulo_documento = "Reporte " + modulo.nombre
    titulo = "Reporte de avance de pruebas " + "'"+modulo.nombre+"'"
    date_time = timezone.datetime.today().strftime('%Y-%m-%d')
    subtitulo = "Fecha de elaboración: " + str(date_time)

    response_file = HttpResponse(content_type='application/pdf')
    response_file['Content-Disposition'] = f'inline; filename="{nombre_archivo}.pdf"'
    # Create bytestream buffer
    pdf = SimpleDocTemplate(
        response_file,
        pagesize=letter
    )
    # pdf.setTitle(titulo_documento)
    elems = []
    styles = getSampleStyleSheet()
    elems.append(Paragraph(titulo, styles["title"]))
    elems.append(Paragraph(subtitulo, styles["title"]))
    elems.append(Spacer(1, 12))

    table_pruebas = Table(data_guiones)

    style = TableStyle([
        # ('BACKGROUND', (0, 0), (3, 0), colors.green),
        # ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),

        ('ALIGN', (0, 0), (-1, -1), 'CENTER'),

        ('FONTNAME', (0, 0), (-1, -1), 'Helvetica'),
        ('FONTSIZE', (0, 0), (-1, -1), 8),

        ('BOTTOMPADDING', (0, 0), (-1, -1), 6),

        ('BACKGROUND', (0, 0), (-1, 0), colors.Color(256/256, 256/256, 256/256)),
    ])

    table_pruebas.setStyle(style)

    rowNumb = len(data_guiones)
    for i in range(1, rowNumb):
        if i % 2 == 0:
            bc = colors.Color(256/256, 256/256, 256/256)
            # bc = colors.burlywood
        else:
            bc = colors.Color(249/256, 242/256, 231/256)
            # bc = colors.beige

        ts = TableStyle(
            [('BACKGROUND', (0, i), (-1, i), bc)]
        )
        table_pruebas.setStyle(ts)

    ts_borders = TableStyle(
        [
            ('BOX', (0, 0), (-1, -1), 1, colors.black),

            ('GRID', (0, 0), (-1, -1), 1, colors.black),
        ]
    )
    table_pruebas.setStyle(ts_borders)

    # elems.append(table_pruebas)
    # elems.append(table_defectos)

    # data = [3, 18, 20]
    # %%%%%%%%%%%%%%%%%%%Pie Chart Liberados%%%%%%%%%%%%%%%%%%%%%
    guion_data = [guion_objects_lib, guion_objects_notlib]
    chart = Pie()
    chart.data = guion_data
    chart.x = 85
    chart.y = 5
    chart.width = 60
    chart.height = 60

    chart.labels = ['Liberados', 'No Liberados']

    chart.sideLabels = True

    chart.slices[0].fillColor = colors.Color(242/256, 137/256, 109/256)
    chart.slices[1].fillColor = colors.Color(213/256, 207/256, 202/256)
    # chart.slices[0].popout = 8

    title = String(
        50, 100,
        'Guiones Liberados',
        fontSize=14
    )

    # legend = Legend()
    # legend.x = 180
    # legend.y = 80
    # legend.alignment = 'right'

    # legend.colorNamePairs = Auto(obj=chart)

    lib_pie = Drawing(240, 120)
    lib_pie.add(title)
    lib_pie.add(chart)
    # lib_pie.add(legend)
    # %%%%%%%%%%%%%%%%%%%Pie Chart Liberados%%%%%%%%%%%%%%%%%%%%%

    # %%%%%%%%%%%%%%%%%%%Pie Chart Guiones%%%%%%%%%%%%%%%%%%%%%
    pruebas_data = [aprobado, no_aprobado, sin_probar, impedimentos]
    chart = Pie()
    chart.data = pruebas_data
    chart.x = 85
    chart.y = 5
    chart.width = 60
    chart.height = 60

    chart.labels = ['Aprobados', 'No Aprobados', 'Sin Probar', 'Impedimentos']

    chart.sideLabels = True

    chart.slices[0].fillColor = colors.Color(158/256, 235/256, 233/256)
    chart.slices[1].fillColor = colors.Color(214/256, 28/256, 89/256)
    chart.slices[2].fillColor = colors.Color(248/256, 241/256, 233/256)
    chart.slices[3].fillColor = colors.Color(249/256, 200/256, 77/256)
    # chart.slices[0].popout = 8

    title = String(
        50, 100,
        'Avance de pruebas',
        fontSize=14
    )

    # legend = Legend()
    # legend.x = 180
    # legend.y = 80
    # legend.alignment = 'right'

    # legend.colorNamePairs = Auto(obj=chart)

    pruebas_pie = Drawing(240, 120)
    pruebas_pie.add(title)
    pruebas_pie.add(chart)
    # pruebas_pie.add(legend)
    # %%%%%%%%%%%%%%%%%%%Pie Chart Guiones%%%%%%%%%%%%%%%%%%%%%

    side_table = Table([
        [lib_pie],
        [pruebas_pie],
    ])

    if defect_objects_total < 1:
        mainTable = Table([
            [Paragraph("Avance de pruebas", styles["title"])],
            [table_pruebas, side_table],
            [Spacer(1, 12)],
            # [drawing_pie, drawing_pie],
            # [Spacer(1, 12)],
            [Paragraph("Sin Defectos", styles["title"])],
        ])
    else:
        data_defectos = [
            ['Nombre de Proyecto', proyecto.nombre],
            ['Nombre de módulo', modulo.nombre],
            ['Total de defectos activos', defect_objects_total],
            ['Defectos con prioridad alta', prior_alta],
            ['Defectos con prioridad media', prior_media],
            ['Defectos con prioridad baja', prior_baja],
        ]
        table_defectos = Table(data_defectos)
        table_defectos.setStyle(style)
        rowNumb = len(data_defectos)
        for i in range(1, rowNumb):
            if i % 2 == 0:
                bc = colors.Color(256/256, 256/256, 256/256)
                # bc = colors.burlywood
            else:
                bc = colors.Color(249/256, 242/256, 231/256)
                # bc = colors.beige

            ts = TableStyle(
                [('BACKGROUND', (0, i), (-1, i), bc)]
            )
            table_defectos.setStyle(ts)
        table_defectos.setStyle(ts)
        table_defectos.setStyle(ts_borders)

        # %%%%%%%%%%%%%%%%%%%Pie Chart Defectos%%%%%%%%%%%%%%%%%%%%%
        defectos_data = [prior_alta, prior_media, prior_baja]
        chart = Pie()
        chart.data = defectos_data
        chart.x = 85
        chart.y = 5
        chart.width = 60
        chart.height = 60

        chart.labels = ['Alta', 'Media', 'Baja']

        chart.sideLabels = True

        chart.slices[0].fillColor = colors.Color(255/256, 107/256, 107/256)
        chart.slices[1].fillColor = colors.Color(199/256, 244/256, 100/256)
        chart.slices[2].fillColor = colors.Color(78/256, 205/256, 196/256)
        # chart.slices[0].popout = 8

        title = String(
            50, 100,
            'Defectos encontrados',
            fontSize=14
        )

        # legend = Legend()
        # legend.x = 180
        # legend.y = 80
        # legend.alignment = 'right'

        # legend.colorNamePairs = Auto(obj=chart)

        defectos_pie = Drawing(240, 120)
        defectos_pie.add(title)
        defectos_pie.add(chart)
        # pruebas_pie.add(legend)
        # %%%%%%%%%%%%%%%%%%%Pie Chart Defectos%%%%%%%%%%%%%%%%%%%%%

        mainTable = Table([
            [Paragraph("Avance de pruebas", styles["title"])],
            [table_pruebas, side_table],
            [Spacer(1, 12)],
            # [drawing_pie, drawing_pie],
            # [Spacer(1, 12)],
            [Paragraph("Defectos", styles["title"])],
            [table_defectos, defectos_pie],
        ])

    elems.append(mainTable)
    try:
        pdf.build(elems)
        response_file.write(pdf)
        return response_file
    except ZeroDivisionError:
        messages.warning(
            request, "No se cuentan con los elementos necesarios para generar un reporte de pruebas")
        return redirect('reportes_index', modulo_id=modulo.id)


@login_required(login_url="/usuarios/login")
@group_required('TestManager')
def reporte_def(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    proyecto = modulo.proyecto
    guion_objects = GuionPruebas.objects.filter(modulo=modulo)
    cp_objects = CasoPrueba.objects.filter(guion__in=guion_objects)

    context = {
        'modulo': modulo,
        'proyecto': proyecto,
        'guion_objects': guion_objects,
        'cp_objects': cp_objects,
    }

    result_objects = ResultadoCasoPrueba.objects.filter(
        caso_prueba__in=cp_objects)
    defect_objects = Defecto.objects.filter(
        resultado__in=result_objects).exclude(estado="Liberado por QA")
    defect_objects_total = defect_objects.count()
    prior_alta = defect_objects.filter(
        resultado__in=result_objects, prioridad="Alta").count()
    prior_media = defect_objects.filter(
        resultado__in=result_objects, prioridad="Media").count()
    prior_baja = defect_objects.filter(
        resultado__in=result_objects, prioridad="Baja").count()

    # Create PDF
    # Head and titles
    nombre_archivo = "Reporte"+modulo.nombre
    titulo_documento = "Reporte " + modulo.nombre
    titulo = "Reporte de defectos " + "'"+modulo.nombre+"'"
    date_time = timezone.datetime.today().strftime('%Y-%m-%d')
    subtitulo = "Fecha de elaboración: " + str(date_time)

    response_file = HttpResponse(content_type='application/pdf')
    response_file['Content-Disposition'] = f'inline; filename="{nombre_archivo}.pdf"'
    # Create bytestream buffer
    pdf = SimpleDocTemplate(
        response_file,
        pagesize=letter
    )
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
    # pdf.setTitle(titulo_documento)
    elems = []
    styles = getSampleStyleSheet()
    elems.append(Paragraph(titulo, styles["title"]))
    elems.append(Paragraph(subtitulo, styles["title"]))
    elems.append(Spacer(1, 12))

# ------------------
    if defect_objects_total < 1:
        mainTable = Table([
            [Paragraph("Defectos", styles["title"])],
            [Paragraph("Sin defectos encontrados", styles["title"])],
        ])
    else:
        data_defectos = [
            ['Nombre de Proyecto', proyecto.nombre],
            ['Nombre de módulo', modulo.nombre],
            ['Total de defectos activos', defect_objects_total],
            ['Defectos con prioridad alta', prior_alta],
            ['Defectos con prioridad media', prior_media],
            ['Defectos con prioridad baja', prior_baja],
        ]
        table_defectos = Table(data_defectos)
        style = TableStyle([
            # ('BACKGROUND', (0, 0), (3, 0), colors.green),
            # ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),

            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),

            ('FONTNAME', (0, 0), (-1, -1), 'Helvetica'),
            ('FONTSIZE', (0, 0), (-1, -1), 8),

            ('BOTTOMPADDING', (0, 0), (-1, -1), 6),

            ('BACKGROUND', (0, 0), (-1, 0), colors.Color(256/256, 256/256, 256/256)),
        ])
        table_defectos.setStyle(style)
        ts_borders = TableStyle(
            [
                ('BOX', (0, 0), (-1, -1), 1, colors.black),

                ('GRID', (0, 0), (-1, -1), 1, colors.black),
            ]
        )
        table_defectos.setStyle(ts_borders)
        rowNumb = len(data_defectos)
        for i in range(1, rowNumb):
            if i % 2 == 0:
                bc = colors.Color(256/256, 256/256, 256/256)
                # bc = colors.burlywood
            else:
                bc = colors.Color(249/256, 242/256, 231/256)
                # bc = colors.beige

            ts = TableStyle(
                [('BACKGROUND', (0, i), (-1, i), bc)]
            )
            table_defectos.setStyle(ts)
        table_defectos.setStyle(ts)
        # %%%%%%%%%%%%%%%%%%%Pie Chart Defectos%%%%%%%%%%%%%%%%%%%%%
        defectos_data = [prior_alta, prior_media, prior_baja]
        chart = Pie()
        chart.data = defectos_data
        chart.x = 85
        chart.y = 5
        chart.width = 60
        chart.height = 60

        chart.labels = ['Alta', 'Media', 'Baja']

        chart.sideLabels = True

        chart.slices[0].fillColor = colors.Color(255/256, 107/256, 107/256)
        chart.slices[1].fillColor = colors.Color(199/256, 244/256, 100/256)
        chart.slices[2].fillColor = colors.Color(78/256, 205/256, 196/256)
        # chart.slices[0].popout = 8

        title = String(
            50, 100,
            'Defectos encontrados',
            fontSize=14
        )

        # legend = Legend()
        # legend.x = 180
        # legend.y = 80
        # legend.alignment = 'right'

        # legend.colorNamePairs = Auto(obj=chart)

        defectos_pie = Drawing(240, 120)
        defectos_pie.add(title)
        defectos_pie.add(chart)
        # pruebas_pie.add(legend)
        # %%%%%%%%%%%%%%%%%%%Pie Chart Defectos%%%%%%%%%%%%%%%%%%%%%

        mainTable = Table([
            [Paragraph("Defectos", styles["title"])],
            [table_defectos, defectos_pie],
        ])

        elems.append(mainTable)
        elems.append(PageBreak())

        def genPinTable(defecto):
            pinElemTable = None

            pinElemWidth = 500
            # 1) Build Structure
            titleTable = Table([
                [Paragraph(defecto.titulo, styles["title"])]
            ], pinElemWidth)

            factsTable = Table([
                ['Estado: ', Paragraph(defecto.estado, styles["Normal"]), 'Tipo defecto: ',
                    Paragraph(defecto.tipo_defecto, styles["Normal"]), 'Prioridad: ', Paragraph(defecto.prioridad, styles["Normal"])]
            ])

            descTable = Table([
                ['Descripcción: ', Paragraph(
                    defecto.descripcion, styles["Normal"])]
            ], [80, 400])

            if defecto.evidencia:
                picture = Image(defecto.evidencia.path)
                picture.drawWidth = 345
                picture.drawHeight = 180
                picTable = Table([[picture]], 345, 180)
            else:
                picTable = Table(
                    [[Paragraph("Sin evidencias", styles["title"])]])

            pinElemTable = Table([
                [titleTable],
                [factsTable],
                [descTable],
                [picTable]
            ], pinElemWidth)

            # 2) Add Style
            '''
            # List available fonts
            from reportlab.pdfgen import canvas
            for font in canvas.Canvas('abc').getAvailableFonts():
                print(font)
            '''
            titleTableStyle = TableStyle([
                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                ('FONTSIZE', (0, 0), (-1, -1), 12),
                ('FONTNAME', (0, 0), (-1, -1),
                    'Helvetica-Bold'
                 ),

                ('TOPPADDING', (0, 0), (-1, -1), 0),
            ])
            titleTable.setStyle(titleTableStyle)

            refNoTableStyle = TableStyle([
                ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                ('FONTNAME', (0, 0), (-1, -1),
                    'Helvetica-Oblique'),

                ('TOPPADDING', (0, 0), (-1, -1), 0),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 5),
            ])
            factsTable.setStyle(refNoTableStyle)

            descTableStyle = TableStyle([
                ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                ('FONTNAME', (0, 0), (-1, -1),
                    'Helvetica-Oblique'),

                ('TOPPADDING', (0, 0), (-1, -1), 0),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 10),
            ])
            descTable.setStyle(descTableStyle)

            picTableStyle = TableStyle([
                ('LEFTPADDING', (0, 0), (-1, -1), 5),

                ('TOPPADDING', (0, 0), (-1, -1), 0),
            ])
            picTable.setStyle(picTableStyle)

            pinElemTableStyle = TableStyle([
                ('BOX', (0, 0), (-1, -1), 2, colors.black),

                ('TOPPADDING', (0, 0), (-1, -1), 0),
                ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
            ])
            pinElemTable.setStyle(pinElemTableStyle)

            return pinElemTable

        elems.append(
            Paragraph("Detalle de defectos encontrados", styles["title"]))
        if prior_alta > 0:
            prior_alta_obj = defect_objects.filter(
                resultado__in=result_objects, prioridad="Alta")
            elems.append(Paragraph("Prioridad alta", styles["title"]))
            elems.append(Spacer(1, 25))
            for index, defecto in enumerate(prior_alta_obj):
                elems.append(genPinTable(defecto))
                if not(index % 2 == 0):
                    elems.append(PageBreak())
                else:
                    elems.append(Spacer(1, 25))
            elems.append(PageBreak())

        if prior_media > 0:
            prior_media_obj = defect_objects.filter(
                resultado__in=result_objects, prioridad="Media")
            elems.append(Paragraph("Prioridad media", styles["title"]))
            elems.append(Spacer(1, 25))
            for index, defecto in enumerate(prior_media_obj):
                elems.append(genPinTable(defecto))
                if not(index % 2 == 0):
                    elems.append(PageBreak())
                else:
                    elems.append(Spacer(1, 25))
            elems.append(PageBreak())
        if prior_baja > 0:
            prior_baja_obj = defect_objects.filter(
                resultado__in=result_objects, prioridad="Baja")
            elems.append(Paragraph("Prioridad baja", styles["title"]))
            elems.append(Spacer(1, 25))
            for index, defecto in enumerate(prior_baja_obj):
                elems.append(genPinTable(defecto))
                if not(index % 2 == 0):
                    elems.append(PageBreak())
                else:
                    elems.append(Spacer(1, 25))
            elems.append(PageBreak())

# -------------------

    try:
        pdf.build(elems)
        response_file.write(pdf)
        return response_file
    except ZeroDivisionError:
        messages.warning(
            request, "No se cuentan con los elementos necesarios para generar un reporte de pruebas")
        return redirect('reportes_index', modulo_id=modulo.id)

    # buf = io.BytesIO()
    # # Create a canvas
    # #c = canvas.Canvas(buf, pagesize=letter, bottomup=0)
    # c = canvas.Canvas(buf, pagesize=letter)
    # c.setFont("Helvetica", 18, leading=None)
    # c.drawCentredString(300, 750, titulo)
    # c.setFont('Helvetica', 14)
    # c.drawCentredString(300, 720, subtitulo)
    # c.line(0, 700, 1000, 700)
    # c.setTitle(titulo_documento)
    # c.showPage()
    # c.save()

    # pdf = buf.getvalue()
    # buf.close()
    # response_file.write(pdf)
    # return response_file

    # return response
    # # Create bytestream buffer
    # buf = io.BytesIO()
    # # Create a canvas
    # c = canvas.Canvas(buf, pagesize=letter, bottomup=0)
    # # create a text object
    # textob = c.beginText()
    # textob.setTextOrigin(inch, inch)
    # textob.setFont("Helvetica", 14)
    # # add lines of text
    # lines = [
    #     "This is line 1",
    #     "This is line 2",
    #     "This is line 3",
    # ]

    # # add lines to text object
    # for line in lines:
    #     textob.textLine(line)

    # # draw the lines
    # c.drawText(textob)
    # # finish the page
    # c.showPage()
    # c.save()
    # buf.seek(0)

    # return FileResponse(buf, as_attachment=True, filename="prueba.pdf")
