from django.shortcuts import render, redirect


def home(request):
    return render(request, 'home.html')


def bienvenida(request):
    return render(request, 'bienvenida.html')
