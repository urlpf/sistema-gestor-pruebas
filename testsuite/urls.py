from django.urls import path, include
from . import views

urlpatterns = [
    # Modulos
    # indice suites
    path('<int:modulo_id>/', views.index_suites, name="index_suites"),
    # crear suite
    path('alta_suite/<int:modulo_id>/',
         views.alta_suite, name="alta_suite"),
    # Detalle de suite
    path('detalle_suite/<int:suite_id>/',
         views.detalle_suite, name="detalle_suite"),
    # Actualizar suite
    path('actualizar_suite/<int:suite_id>/',
         views.actualizar_suite, name='actualizar_suite'),
    # Borrar suite
    path('borrar_suite/<int:suite_id>/',
         views.borrar_suite, name='borrar_suite'),
    # Agregar Pruebas
    path('agregar_pruebas/<int:suite_id>/',
         views.agregar_pruebas, name='agregar_pruebas'),
    # Quitar Pruebas
    path('quitar_pruebas/<int:suite_id>/',
         views.quitar_pruebas, name='quitar_pruebas'),
]
