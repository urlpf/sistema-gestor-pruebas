from django.contrib.postgres.fields import ArrayField, HStoreField
from django.db import models
from proymod.models import Modulo

# Create your models here.


class TestSuite(models.Model):
    titulo_suite = models.CharField(max_length=150)
    fecha = models.CharField(max_length=15)
    entorno_servidor = models.CharField(max_length=150, blank=True, null=True)
    usuario_acceso = models.CharField(max_length=150, blank=True, null=True)
    contrasena_acceso = models.CharField(max_length=150, blank=True, null=True)
    ram_equipo = models.CharField(max_length=15, blank=True, null=True)
    procesador_equipo = models.CharField(max_length=50, blank=True, null=True)
    so_equipo = models.CharField(max_length=50, blank=True, null=True)
    capacidad_equipo = models.CharField(max_length=15, blank=True, null=True)
    resolucion_equipo = models.CharField(max_length=20, blank=True, null=True)
    tester = models.CharField(max_length=150)
    guiones_pruebas = ArrayField(models.CharField(
        max_length=200), blank=True, null=True)
    notas = models.TextField(max_length=300, blank=True, null=True)
    modulo = models.ForeignKey(Modulo, on_delete=models.CASCADE)

    class Meta:
        unique_together = ["titulo_suite", "modulo"]

    def __str__(self):
        return self.titulo_suite
