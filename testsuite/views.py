from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.contrib import messages
#from .forms import BasePruebaForm, ActualizarGuionForm, RegistrarResultadoForm, RegistrarDefectoForm, ActualizarDefectoForm
from django.contrib.auth.models import User, Group
from .models import TestSuite
from proymod.models import Modulo, Proyecto
from guiprub.models import GuionPruebas, CasoPrueba, Defecto
from django.utils import timezone
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from django.db import IntegrityError
from django.utils.datastructures import MultiValueDictKeyError
from django.core.exceptions import MultipleObjectsReturned, ValidationError
from datetime import datetime
# Create your views here.

# Mensajes de este módulo
mensajes = {
    '001': 'El nombre ingresado ya está registrado en el sistema. Por favor ingresa otro nombre. ',
    '002': 'Elemento registrado exitosamente.',
    '003': 'Elemento modificado exitosamente',
    '004': 'Elemento eliminado exitosamente',
    '005': 'Operación no realizada',
    '006': 'Campos requeridos no ingresados',

}

# Indice de proyectos


@login_required(login_url="/usuarios/login")
def index_suites(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    proyecto = modulo.proyecto

    suite_objects = TestSuite.objects.filter(modulo=modulo.id)

    # Codigo para buscar
    item_name = request.GET.get('item_name')
    if item_name != '' and item_name is not None:
        suite_objects = suite_objects.filter(titulo_suite__icontains=item_name)

    # Codigo del paginador
    paginator = Paginator(suite_objects, 5)
    page = request.GET.get('page')
    suite_objects = paginator.get_page(page)

    context = {
        'suite_objects': suite_objects,
        'modulo': modulo,
        'proyecto': proyecto,
    }

    return render(request, 'testsuites/index_suites.html', context)


@login_required(login_url="/usuarios/login")
def alta_suite(request, modulo_id):
    modulo = Modulo.objects.get(id=modulo_id)
    users = User.objects.all()
    user_lists = []
    for x in users:
        user_lists.append(x.username)
    context = {
        'modulo': modulo,
        'user_lists': user_lists,

    }
    if request.method == 'POST':
        print(request.body)
        try:
            titulo = str(request.POST['titulo'])
            fecha = str(request.POST['fecha'])
            tester = str(request.POST['tester'])
            acceso = int(request.POST['acceso'])
            entorno = int(request.POST['entorno'])
            notas = str(request.POST['notas'])
        except MultiValueDictKeyError:
            titulo = False
            fecha = False
            entorno = False
            tester = False
            acceso = False
        except ValueError:
            titulo = False
            fecha = False
            entorno = False
            tester = False
            acceso = False
        else:
            # if titulo and isValidDate and tester:
            if titulo and fecha and tester:
                if acceso == 1 and entorno == 1:
                    try:
                        usuario = str(request.POST['usuario'])
                        contrasena = str(request.POST['contrasena'])
                        entorno = str(request.POST['direccion'])
                        ram = str(request.POST['ram'])
                        procesador = str(request.POST['procesador'])
                        sisope = str(request.POST['sisope'])
                        discod = str(request.POST['discod'])
                        resolucion = str(request.POST['resolucion'])
                    except ValueError:
                        usuario = False
                        contrasena = False
                        entorno = False
                        ram = False
                        procesador = False
                        sisope = False
                        discod = False
                        resolucion = False
                    else:
                        try:
                            TestSuite.objects.create(
                                titulo_suite=titulo,
                                fecha=fecha,
                                entorno_servidor=entorno,
                                usuario_acceso=usuario,
                                contrasena_acceso=contrasena,
                                ram_equipo=ram,
                                procesador_equipo=procesador,
                                so_equipo=sisope,
                                capacidad_equipo=discod,
                                resolucion_equipo=resolucion,
                                tester=tester,
                                notas=notas,
                                modulo=modulo,
                            )
                        except IntegrityError as err:
                            messages.warning(
                                request, "El titulo ingresado ya esta registrado en el sistema")
                            return redirect('index_suites', modulo_id=modulo.id)
                        else:
                            messages.success(request, mensajes.get("002"))
                            return redirect('index_suites', modulo_id=modulo.id)
                elif acceso == 1 and entorno == 0:
                    try:
                        usuario = str(request.POST['usuario'])
                        contrasena = str(request.POST['contrasena'])
                        entorno = str(request.POST['direccion'])
                    except ValueError:
                        usuario = False
                        contrasena = False
                        entorno = False
                    else:
                        try:
                            TestSuite.objects.create(
                                titulo_suite=titulo,
                                fecha=fecha,
                                entorno_servidor=entorno,
                                usuario_acceso=usuario,
                                contrasena_acceso=contrasena,
                                tester=tester,
                                notas=notas,
                                modulo=modulo,
                            )
                        except IntegrityError as err:
                            messages.warning(
                                request, "El titulo ingresado ya esta registrado en el sistema")
                            return redirect('index_suites', modulo_id=modulo.id)
                        else:
                            messages.success(request, mensajes.get("002"))
                            return redirect('index_suites', modulo_id=modulo.id)
                elif acceso == 0 and entorno == 1:
                    try:
                        ram = str(request.POST['ram'])
                        procesador = str(request.POST['procesador'])
                        sisope = str(request.POST['sisope'])
                        discod = str(request.POST['discod'])
                        resolucion = str(request.POST['resolucion'])
                    except ValueError:
                        ram = False
                        procesador = False
                        sisope = False
                        discod = False
                        resolucion = False
                    else:

                        try:
                            TestSuite.objects.create(
                                titulo_suite=titulo,
                                fecha=fecha,
                                ram_equipo=ram,
                                procesador_equipo=procesador,
                                so_equipo=sisope,
                                capacidad_equipo=discod,
                                resolucion_equipo=resolucion,
                                tester=tester,
                                notas=notas,
                                modulo=modulo,
                            )
                        except IntegrityError as err:
                            messages.warning(
                                request, "El titulo ingresado ya esta registrado en el sistema")
                            return redirect('index_suites', modulo_id=modulo.id)
                        else:
                            messages.success(request, mensajes.get("002"))
                            return redirect('index_suites', modulo_id=modulo.id)
                elif acceso == 0 and entorno == 0:
                    try:
                        TestSuite.objects.create(
                            titulo_suite=titulo,
                            fecha=fecha,
                            tester=tester,
                            notas=notas,
                            modulo=modulo,
                        )
                    except IntegrityError as err:
                        messages.warning(
                            request, "El titulo ingresado ya esta registrado en el sistema")
                        return redirect('index_suites', modulo_id=modulo.id)
                    else:
                        messages.success(request, mensajes.get("002"))
                        return redirect('index_suites', modulo_id=modulo.id)
            else:
                messages.warning(request, mensajes.get("006"))
                return redirect('index_suites', modulo_id=modulo.id)

    return render(request, 'testsuites/alta_suite.html', context)


@login_required(login_url="/usuarios/login")
def detalle_suite(request, suite_id):
    suite = get_object_or_404(TestSuite, pk=suite_id)
    modulo = suite.modulo
    proyecto = modulo.proyecto
    guion_objects = GuionPruebas.objects.filter(suite=suite)
    cp_objects = CasoPrueba.objects.filter(guion__in=guion_objects)
    # Codigo para buscar
    item_name = request.GET.get('estado')
    if item_name != '' and item_name is not None:
        cp_objects = cp_objects.filter(resultado=item_name)
    # elif item_name == "Todos":
    #     cp_objects = CasoPrueba.objects.filter(guion__in=guion_objects)
    context = {
        "suite": suite,
        "modulo": modulo,
        "proyecto": proyecto,
        "cp_objects": cp_objects,
        "guion_objects": guion_objects,
    }
    return render(request, 'testsuites/detalle_suite.html', context)


@login_required(login_url="/usuarios/login")
def actualizar_suite(request, suite_id):
    suite = get_object_or_404(TestSuite, pk=suite_id)
    modulo = suite.modulo
    users = User.objects.all()
    user_lists = []
    for x in users:
        user_lists.append(x.username)

    context = {
        "suite": suite,
        "modulo": modulo,
        "user_lists": user_lists,
    }
    if request.method == 'POST':
        print(request.body)
        try:
            titulo = str(request.POST['titulo'])
            fecha = request.POST['fecha']
            tester = str(request.POST['tester'])
            acceso = int(request.POST['acceso'])
            entorno = int(request.POST['entorno'])
            notas = str(request.POST['notas'])
        except MultiValueDictKeyError:
            titulo = False
            fecha = False
            entorno = False
            tester = False
            acceso = False
        except ValueError:
            titulo = False
            fecha = False
            entorno = False
            tester = False
            acceso = False
        else:
            if titulo and fecha and tester:
                if acceso == 1 and entorno == 1:
                    try:
                        usuario = str(request.POST['usuario'])
                        contrasena = str(request.POST['contrasena'])
                        entorno = str(request.POST['direccion'])
                        ram = str(request.POST['ram'])
                        procesador = str(request.POST['procesador'])
                        sisope = str(request.POST['sisope'])
                        discod = str(request.POST['discod'])
                        resolucion = str(request.POST['resolucion'])

                    except ValueError:
                        usuario = False
                        contrasena = False
                        entorno = False
                        ram = False
                        procesador = False
                        sisope = False
                        discod = False
                        resolucion = False
                    else:
                        try:
                            suite.titulo_suite = titulo
                            suite.fecha = fecha
                            suite.entorno_servidor = entorno
                            suite.usuario_acceso = usuario
                            suite.contrasena_acceso = contrasena
                            suite.ram_equipo = ram
                            suite.procesador_equipo = procesador
                            suite.so_equipo = sisope
                            suite.capacidad_equipo = discod
                            suite.resolucion_equipo = resolucion
                            suite.tester = tester
                            suite.notas = notas
                            suite.modulo = modulo
                            suite.save()
                        except IntegrityError as err:
                            messages.warning(
                                request, "El titulo ingresado ya esta registrado en el sistema")
                            return redirect('detalle_suite', suite_id=suite.id)
                        else:
                            messages.success(request, mensajes.get("003"))
                            return redirect('detalle_suite', suite_id=suite.id)
                elif acceso == 1 and entorno == 0:
                    try:
                        usuario = str(request.POST['usuario'])
                        contrasena = str(request.POST['contrasena'])
                        entorno = str(request.POST['direccion'])
                    except ValueError:
                        usuario = False
                        contrasena = False
                        entorno = False
                    else:
                        try:
                            suite.titulo_suite = titulo
                            suite.fecha = fecha
                            suite.entorno_servidor = entorno
                            suite.usuario_acceso = usuario
                            suite.contrasena_acceso = contrasena
                            suite.tester = tester
                            suite.notas = notas
                            suite.modulo = modulo
                            suite.save()
                        except IntegrityError as err:
                            messages.warning(
                                request, "El titulo ingresado ya esta registrado en el sistema")
                            return redirect('detalle_suite', suite_id=suite.id)
                        else:
                            messages.success(request, mensajes.get("003"))
                            return redirect('detalle_suite', suite_id=suite.id)
                elif acceso == 0 and entorno == 1:
                    try:
                        ram = str(request.POST['ram'])
                        procesador = str(request.POST['procesador'])
                        sisope = str(request.POST['sisope'])
                        discod = str(request.POST['discod'])
                        resolucion = str(request.POST['resolucion'])
                    except ValueError:
                        ram = False
                        procesador = False
                        sisope = False
                        discod = False
                        resolucion = False
                    else:
                        try:
                            suite.titulo_suite = titulo
                            suite.fecha = fecha
                            suite.ram_equipo = ram
                            suite.procesador_equipo = procesador
                            suite.so_equipo = sisope
                            suite.capacidad_equipo = discod
                            suite.resolucion_equipo = resolucion
                            suite.tester = tester
                            suite.notas = notas
                            suite.modulo = modulo
                            suite.save()
                        except IntegrityError as err:
                            messages.warning(
                                request, "El titulo ingresado ya esta registrado en el sistema")
                            return redirect('detalle_suite', suite_id=suite.id)
                        else:
                            messages.success(request, mensajes.get("003"))
                            return redirect('detalle_suite', suite_id=suite.id)
                elif acceso == 0 and entorno == 0:
                    try:
                        suite.titulo_suite = titulo
                        suite.fecha = fecha
                        suite.tester = tester
                        suite.notas = notas
                        suite.modulo = modulo
                        suite.save()
                    except IntegrityError as err:
                        messages.warning(
                            request, "El titulo ingresado ya esta registrado en el sistema")
                        return redirect('detalle_suite', suite_id=suite.id)
                    else:
                        messages.success(request, mensajes.get("003"))
                        return redirect('detalle_suite', suite_id=suite.id)
            else:
                messages.warning(request, mensajes.get("006"))
                return redirect('detalle_suite', suite_id=suite.id)
    return render(request, 'testsuites/actualizar_suite.html', context)


@login_required(login_url="/usuarios/login")
def borrar_suite(request, suite_id):
    suite = get_object_or_404(TestSuite, pk=suite_id)
    modulo = suite.modulo

    if request.method == 'POST':
        suite.delete()
        messages.success(request, mensajes.get("004"))
        return redirect('index_suites', modulo_id=modulo.id)

    context = {
        'suite': suite,
        'modulo': modulo,
    }
    return render(request, 'testsuites/borrar_suite.html', context)


@login_required(login_url="/usuarios/login")
def agregar_pruebas(request, suite_id):
    suite = get_object_or_404(TestSuite, pk=suite_id)
    modulo = suite.modulo
    guion_objects = GuionPruebas.objects.filter(modulo=modulo.id)
    list_guiones = []
    for x in guion_objects:
        if x.suite:
            pass
        else:
            list_guiones.append(x.nombre)
            print(x.nombre)
    if len(list_guiones) < 1:
        messages.warning(
            request, "No hay guiones de pruebas disponibles para agregar")
        return redirect('detalle_suite', suite_id=suite.id)
    context = {
        'suite': suite,
        'modulo': modulo,
        'list_guiones': list_guiones,
    }
    if request.method == 'POST':
        try:
            list_guiones = [(str(n), str(v)) for n, v in request.POST.items()
                            if n.startswith('guion')]
            for index, value in enumerate(list_guiones):
                list_guiones[index] = value[1]
            list_guiones = set(list_guiones)
            list_guiones = list(list_guiones)
        except ValueError:
            list_guiones = False
        if list_guiones:
            if not(suite.guiones_pruebas):
                suite.guiones_pruebas = list_guiones
                guion_objects = GuionPruebas.objects.filter(
                    nombre__in=list_guiones)
                for x in guion_objects:
                    x.suite = suite
                    x.save()
                suite.save()
                messages.success(request, mensajes.get("003"))
                return redirect('detalle_suite', suite_id=suite.id)
            else:
                current_list = list(suite.guiones_pruebas)
                current_list.extend(list_guiones)
                current_list = set(current_list)
                current_list = list(current_list)
                suite.guiones_pruebas = current_list
                guion_objects = GuionPruebas.objects.filter(
                    nombre__in=current_list)
                for x in guion_objects:
                    x.suite = suite
                    x.save()
                suite.save()
                messages.success(request, mensajes.get("003"))
                return redirect('detalle_suite', suite_id=suite.id)

    return render(request, 'testsuites/agregar_pruebas.html', context)


@login_required(login_url="/usuarios/login")
def quitar_pruebas(request, suite_id):
    suite = get_object_or_404(TestSuite, pk=suite_id)
    modulo = suite.modulo
    # guion_objects = GuionPruebas.objects.filter(modulo=modulo.id)
    suite_guiones = suite.guiones_pruebas
    context = {
        'suite': suite,
        'modulo': modulo,
        'suite_guiones': suite_guiones,
    }
    if request.method == 'POST':
        list_guiones = [(str(n), str(v)) for n, v in request.POST.items()
                        if n.startswith('guion')]
        for index, value in enumerate(list_guiones):
            list_guiones[index] = value[1]
        list_guiones = set(list_guiones)
        list_guiones = list(list_guiones)
        guion_objects = GuionPruebas.objects.filter(
            nombre__in=list_guiones)
        for x in guion_objects:
            x.suite = None
            x.save()
        for index, value in enumerate(suite_guiones):
            if value in list_guiones:
                suite_guiones.pop(index)
        suite.guiones_pruebas = suite_guiones
        suite.save()
        messages.success(request, mensajes.get("003"))
        return redirect('detalle_suite', suite_id=suite.id)

    return render(request, 'testsuites/quitar_pruebas.html', context)
