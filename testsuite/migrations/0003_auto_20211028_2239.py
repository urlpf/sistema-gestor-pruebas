# Generated by Django 3.1.7 on 2021-10-29 03:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('proymod', '0008_auto_20210630_1343'),
        ('testsuite', '0002_auto_20211028_2237'),
    ]

    operations = [
        migrations.RenameField(
            model_name='testsuite',
            old_name='titulo',
            new_name='titulo_suite',
        ),
        migrations.AlterUniqueTogether(
            name='testsuite',
            unique_together={('titulo_suite', 'modulo')},
        ),
    ]
