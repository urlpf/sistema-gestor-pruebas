from django.contrib import admin
from .models import TestSuite
from django import forms
from django_admin_hstore_widget.forms import HStoreFormField


class TestSuiteAdminForm(forms.ModelForm):
    entorno_cliente = HStoreFormField()

    class Meta:
        model = TestSuite
        exclude = ()


@admin.register(TestSuite)
class TestSuiteAdmin(admin.ModelAdmin):
    form = TestSuiteAdminForm
# Register your models here.
#
